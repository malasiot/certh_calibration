#include <iostream>
#include <fstream>
#include <opencv2/highgui/highgui.hpp>
#include <camera_helpers/OpenNICapture.h>
#include <camera_helpers/OpenNIServiceClient.h>
#include <robot_helpers/Robot.h>
#include <robot_helpers/Geometry.h>
#include <robot_helpers/Utils.h>

using namespace std ;

bool cont;
int cx, cy;
bool stop=false;


float points[9][3] = {
-0.2, 	-0.9,   1.4,
-0.0,   -0.9,   1.4,
0.2,  	-0.9,   1.4,
-0.2, 	-1,     1.1,
-0.0,   -1, 	1.1,
0.2,  	-1, 	1.1,
-0.2, 	-1.1, 	0.8,
-0.0,   -1.1, 	0.8,
0.2,  	-1.1, 	0.8
} ;

void mouse_callback( int event, int x, int y, int flags, void* param){

    if(event == CV_EVENT_LBUTTONDOWN){
        cx = x;
        cy = y;
        cont = true;
    }
    if(event == CV_EVENT_RBUTTONDOWN){
        stop=true;
    }

}

void grabpc()
{
    robot_helpers::Robot rb;

    rb.moveGripperToPose("r1", Vector3d(-1, -0.5, 1), robot_helpers::lookAt(Vector3d(0, 0, -1))) ;


    cv::Mat rgb, depth ;
    pcl::PointCloud<pcl::PointXYZ> pc ;
    ros::Time ts ;
    image_geometry::PinholeCameraModel cm ;

    cv::Mat meanRobot = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));
    for (unsigned int i=0;i<9;i++){
        for (unsigned int j=0;j<3;j++){

            meanRobot.at<float>(j, 0) += (points[i][j]/9.0f);
        }
    }

    //~ Seting the Points and move the robot
    cvNamedWindow("calibration");
    cvSetMouseCallback( "calibration", mouse_callback, NULL);

    float xtionPoints[9][3];
    cv::Mat meanXtion = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));

    Eigen::Quaterniond q = robot_helpers::lookAt(Vector3d(-1, 0, 0), M_PI);

    for (unsigned int i=0;i<9;i++){

        robot_helpers::moveArm(rb, Eigen::Vector3d(points[i][0],points[i][1],points[i][2]),   q, "r2" ) ;

        if ( camera_helpers::openni::grab( "xtion3" ,rgb, depth, pc, ts, cm) )
        {
            cont = false;

            while(!cont){

                cv::imshow("calibration", rgb);
                int k = cv::waitKey(1);
                if(stop)
                    return ;

            }

            pcl::PointXYZ p = pc.at(cx, cy);

            xtionPoints[i][0] = p.x;
            xtionPoints[i][1] = p.y;
            xtionPoints[i][2] = p.z;

            cout << "Xtion points: " << cx << ' ' << cy << ' ' << p.x << " " << p.y << " " << p.z << std::endl;

            meanXtion.at<float>(0, 0) += p.x/9.0f;
            meanXtion.at<float>(1, 0) += p.y/9.0f;
            meanXtion.at<float>(2, 0) += p.z/9.0f;

        }
        else
        {
            ROS_ERROR("Failed to call service");
            return ;
        }
    }

    cv::Mat H = cv::Mat(3, 3, CV_32FC1, cv::Scalar::all(0));
    cv::Mat Pa = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));
    cv::Mat Pb = cv::Mat(1, 3, CV_32FC1, cv::Scalar::all(0));

    for(int i=0; i<9; ++i){

        for(int j=0; j<3; ++j){

            Pa.at<float>(j, 0) = xtionPoints[i][j] - meanXtion.at<float>(j, 0);
            Pb.at<float>(0, j) = points[i][j] - meanRobot.at<float>(j, 0);

        }

        H += Pa * Pb;
    }


    cv::SVD svd(H, cv::SVD::FULL_UV);
    cv::Mat tr(4, 4, CV_32FC1, cv::Scalar::all(0)) ;
    cv::Mat V = svd.vt.t();
    double det = cv::determinant(V);

    if(det < 0){
        for(int i=0; i<V.rows; ++i)
            V.at<float>(i,3) *= -1;
    }

    cv::Mat R = V * svd.u.t();
    cv::Mat t = (-1)*R*meanXtion + meanRobot;

    cout << meanXtion << ' ' << meanRobot << endl ;
    cout << R << t << endl ;
    cout << H << endl ;
    for(int i=0; i<3; ++i)
        for(int j=0; j<3; ++j)
            tr.at<float>(i, j) = R.at<float>(i, j) ;

    for(int i=0 ; i<3 ; i++)
        tr.at<float>(i, 3) = t.at<float>(i, 0) ;

    tr.at<float>(3, 3) = 1.0 ;

    cv::Mat trInv = tr.inv() ;

    string outFolder = getenv("HOME") ;
    outFolder += "/.ros/clopema_calibration" ;
    system(("mkdir -p " + outFolder).c_str());
    outFolder += "/handeye_rgb.calib";
    ofstream fout(outFolder.c_str());

    fout << "2\n" ;
    fout << "xtion3_link_ee\n" ;

    for(int i=0; i<4; ++i){
        for(int j=0; j<4; ++j)
            fout << trInv.at<float>(i, j) << " " ;
        fout << endl;
    }

     cout << trInv << endl;

    camera_helpers::openni::disconnect("xtion3") ;

    rb.setServoPowerOff() ;
}

int main(int argc, char **argv) {

    ros::init(argc, argv, "move_pose_dual");
    ros::NodeHandle nh;
    camera_helpers::openni::connect("xtion3") ;
    grabpc();

}
