#include <camera_helpers/gphoto2_capture.hpp>
#include <robot_helpers/robot.hpp>
#include <robot_helpers/geometry.hpp>
#include <cvx/util/math/rng.hpp>
#include <cvx/util/misc/cv_helpers.hpp>
#include <cvx/util/misc/path.hpp>
#include <cvx/util/misc/strings.hpp>

#include <fstream>

#include <opencv2/highgui/highgui.hpp>

#include <ros/ros.h>

using namespace std ;
using namespace robot_helpers ;
using namespace cvx::util ;
using namespace Eigen ;

const int n_stations = 100 ;
uint current = 0 ;

void guiCallBack(int event, int x, int y, int flags, void* userdata)
{
     if  ( event == cv::EVENT_LBUTTONDOWN )
         current ++ ;
}

Matrix4d Rq(double x, double y, double z, double w, double tx, double ty, double tz)
{
    Matrix4d res ;

    float n22 = sqrt(x*x + y*y + z*z + w*w) ;

    x /= n22 ; y /= n22 ; z /= n22 ; w /= n22 ;

    res << 1 - 2*(y*y+z*z), 2*(x*y-z*w), 2*(x*z+y*w), tx,
           2*(x*y+z*w), 1-2*(x*x+z*z), 2*(y*z-x*w), ty,
           2*(x*z-y*w), 2*(y*z+x*w),   1-2*(x*x+y*y), tz,
           0, 0, 0, 1;

    return res ;
}

int main(int argc, char *argv[]) {

    ros::init(argc, argv, "handeye_capture_overhead_camera");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    std::string home = getenv("HOME");

    cv::namedWindow("gphoto2_capture", cv::WINDOW_NORMAL) ;
    //cv::setMouseCallback("gphoto2_capture", guiCallBack) ;

    string arm_name = "r2" ;

    RobotArm arm(arm_name) ;

    arm.moveHome() ;

    RNG rng ;

    const double minX = -0.15;  
    const double maxX = 0.3 ;   
    const double minY = -0.9 ;
    const double maxY = -1.2;
    const double minZ = 0.5 ; 
    const double maxZ = 1.1 ;  

    //ofstream position_file;
    //position_file.open ("/tmp/position_file.txt");

    double X, Y, Z;
    Quaterniond q;

    while ( current <n_stations ) {
        ros::Duration(0.5).sleep() ;
        // move hand to random position
        while (1)
        {
            X = rng.uniform(minX, maxX) ;
            Y = rng.uniform(minY, maxY) ;
            Z = rng.uniform(minZ, maxZ) ;

            const double qscale = 0.5 ;
            double qx = qscale * rng.uniform(-0.5, 0.5) ;
            double qy = qscale * rng.uniform(-0.5, 0.5) ;
            double qz = qscale * rng.uniform(-0.5, 0.5) ;
            double qw = qscale * rng.uniform(-0.5, 0.5) ;

            q = robot_helpers::lookAt(Vector3d(-1, 0, 0), -M_PI/2) ; 
            q = Quaterniond(q.x() + qx, q.y() + qy, q.z() + qz, q.w() + qw) ;
            q.normalize();

            RobotArm::Plan plan ;
            if ( !arm.planTipIK(Eigen::Vector3d(X, Y, Z), q, plan) ) {
                cerr << "can't plan to location:" << Vector3d(X, Y, Z).adjoint() << endl ;
                continue ;
            }
            else {
                if ( arm.execute(plan) ) {
                    cout << "moving to: " << Eigen::Vector3d(X, Y, Z).adjoint() << endl  ;
                    cout << "tip at: " << arm.getTipPose().translation().adjoint() <<endl  ;
                    break ;
                }
            }

        }

        //Matrix4d mat = Rq(q.x(), q.y(), q.z(), q.z(), X, Y, Z);
	//frame alvar_up_left
	Matrix4d mat = getTransform("alvar_r2_leftUp", "base_link").matrix();


        // capture image

        cv::Mat im ;

	ros::Duration(1).sleep();

//        im = cv::imread("/tmp/cap.png") ;

//        cv::imshow("gphoto2_capture", im) ;

//        cv::waitKey() ;


       if ( camera_helpers::gphoto2::capture(im, "nikon_overhead") )
        {
            cv::imshow("gphoto2_capture", im) ;
            char c = (char)cv::waitKey(0);
	    if (c == '1')
            {   
		std::string txt_path = home + std::string("/.ros/data/certh_data/calibration_nikon/data_extrinsics/");
                Path dir_to_txt(txt_path.c_str(), cvx::util::format("pose%05d.txt", current));
                ofstream position_file;
                position_file.open(dir_to_txt.toString());
                if (position_file.is_open())
                {
		    std::string impath = home + std::string("/.ros/data/certh_data/calibration_nikon/data_extrinsics/cap%05d.png");
                    imwritef(im, impath.c_str(), current) ;
                    position_file << mat << endl;
                    cout << "Paramemeters written" << endl;
                    current++;
                    position_file.close();
                }
                else
                {
                    cerr << "Could not open the file" << endl;
                }

            }

        }
        else {
            cerr << "Could not query photo/capture service" << endl ;
        }

    }

        arm.setServoPowerOff();



}

