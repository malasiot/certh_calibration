#include <ros/ros.h>

#include <cvx/camera/calibration.hpp>
#include <cvx/camera/pose.hpp>
#include <cvx/camera/handeye.hpp>
#include <cvx/util/misc/arg_parser.hpp>
#include <cvx/util/misc/path.hpp>

#include <regex>

using namespace cvx::util ;
using namespace cvx::camera ;
using namespace std ;
using namespace Eigen ;

static void drawCube(const cv::Mat &im, const PinholeCamera &cam, const Affine3d &pose, float sz, const string &out_path) {
    vector<cv::Point3f> cube { { 0, 0, 0 }, { 0, sz, 0 }, { sz, sz, 0 }, {sz, 0, 0},
                               { 0, 0, sz }, { 0, sz, sz }, { sz, sz, sz }, {sz, 0, sz} } ;

    vector<cv::Point> cube_pj ;

    for( const cv::Point3f &p: cube ) {
        Vector3d cs = pose * Vector3d(p.x, p.y, p.z) ;
        cv::Point2d pj = cam.project(cv::Point3d(cs.x(), cs.y(), cs.z())) ;
        cube_pj.push_back(cv::Point(pj.x, pj.y)) ;
    }

    cv::line(im, cube_pj[0], cube_pj[1], cv::Scalar(255, 0, 0), 3) ;
    cv::line(im, cube_pj[1], cube_pj[2], cv::Scalar(255, 0, 0), 3) ;
    cv::line(im, cube_pj[2], cube_pj[3], cv::Scalar(255, 0, 0), 3) ;
    cv::line(im, cube_pj[3], cube_pj[0], cv::Scalar(255, 0, 0), 3) ;

    cv::line(im, cube_pj[4], cube_pj[5], cv::Scalar(255, 0, 255), 3) ;
    cv::line(im, cube_pj[5], cube_pj[6], cv::Scalar(255, 0, 255), 3) ;
    cv::line(im, cube_pj[6], cube_pj[7], cv::Scalar(255, 0, 255), 3) ;
    cv::line(im, cube_pj[7], cube_pj[4], cv::Scalar(255, 0, 255), 3) ;

    cv::line(im, cube_pj[0], cube_pj[4], cv::Scalar(0, 0, 255), 3) ;
    cv::line(im, cube_pj[1], cube_pj[5], cv::Scalar(0, 0, 255), 3) ;

    cv::line(im, cube_pj[3], cube_pj[7], cv::Scalar(0, 200, 255), 3) ;
    cv::line(im, cube_pj[2], cube_pj[6], cv::Scalar(0, 200, 255), 3) ;


    cv::imwrite(out_path, im) ;

}

void find_target_motions(const PinholeCamera &cam, const CameraCalibration::Data &cdata,  const vector<Matrix4d> &extrinsics,
                         vector<Affine3d> &gripper_to_base, vector<Affine3d> &target_to_sensor )
{
    const string img_file_prefix = "cap" ;
    const string img_file_suffix = "png" ;
    const string pose_file_prefix = "pose" ;
    const string pose_file_suffix = "txt" ;

    std::string file_name_regex =  img_file_prefix + "([[:digit:]]+)\\." + img_file_suffix ;

    std::regex rx(file_name_regex) ;

    for ( uint k=0 ; k<cdata.image_paths_.size() ; k++ ) {

       uint n_markers = cdata.markers_[k].size() ;

    //   if ( n_markers < 40 ) continue ;

       vector<cv::Point3f> object_pts ;
       vector<cv::Point2f> image_pts ;
       object_pts.resize(n_markers) ;
       image_pts.resize(n_markers) ;


        for( size_t j=0 ; j < n_markers ; j++ ) {
            const auto &m = cdata.markers_[k][j] ;
            int idx = m.id_ ;
            object_pts[j] = cdata.coords_[idx] ;
            image_pts[j] = m.pt_ ;
        }

        double err ;
       Eigen::Affine3d pose = estimatePosePlanar(image_pts, object_pts, cam, err) ;
     //   Eigen::Affine3d a, pose = estimatePose(image_pts, object_pts, cam, a, false, err) ;

        Path ipath(cdata.image_paths_[k]) ;
   //    cv::Mat im ;
   //    im = cv::imread(cdata.image_paths_[k]) ;
   //    drawCube(im, cam, pose, 0.04, "/tmp/" + ipath.name()) ;

     //   if ( err > 0.1 ) continue ;

        cout << err << endl ;

        target_to_sensor.push_back(pose) ;

        //target_to_sensor.push_back(Affine3d(extrinsics[k])) ;


        string folder = ipath.parent() ;
        string iname = ipath.name() ;

        std::smatch sm ;

        // test of this is a valid filename

        if ( !std::regex_match(iname, sm, rx ) ) continue ;

        string sp = sm[1] ;

        std::string pose_file_name = pose_file_prefix + sp + "." + pose_file_suffix ;

        cout << pose_file_name << endl ;

        ifstream strm(folder + '/' + pose_file_name) ;

        Matrix4d tr ;

        for (int row = 0; row < 4; row++)
               for (int col = 0; col < 4; col++) {
                   strm >> tr(row, col) ;
               }

        gripper_to_base.push_back(Affine3d(tr)) ;

    }
}

void print_gripper_locations(const PinholeCamera &cam, const CameraCalibration::Data &cdata, const vector<Affine3d> &gripper_to_base, const Affine3d &sensor_to_base) {
    for( uint i=0 ; i<cdata.image_paths_.size() ; i++ )  {
        Vector3d p = sensor_to_base.inverse() * gripper_to_base[i].translation() ;

        cv::Point2d pj = cam.project(cv::Point3d(p.x(), p.y(), p.z())) ;
        cout << cdata.image_paths_[i] << "--> " << pj << endl ;

    }



}

istream &operator >> (istream &strm, cv::Size &sz) {
    strm >> sz.width >> sz.height ;
    return strm ;
}

int main(int argc, char *argv[]) {

    ArgumentParser args ;

    string data_folder, camera_intrinsics ;
    bool compute_markers = false ;
    cv::Size pattern_grid(4, 5) ;
    float pattern_width = 0.04, pattern_gap = 0.01 ;
    bool print_help ;

    args.addOption("-h|--help", print_help, true).setMaxArgs(0).setDescription("print this help message") ;
    args.addOption("--data", data_folder).required().setName("<folder>").setDescription("Folder with captured images and robot tip poses") ;
    args.addOption("--markers", compute_markers, true).setMaxArgs(0).setDescription("Recompute markers positions on images") ;
    args.addOption("--grid", pattern_grid).setMinArgs(2).setMaxArgs(2).setName("<gx> <gy>").setDescription("Number of tags in x and y dimensions") ;
    args.addOption("--width", pattern_width).setName("<arg>").setDescription("Width of the AprilTag square in meters") ;
    args.addOption("--gap", pattern_gap).setName("<arg>").setDescription("Gap between AprilTags in the grid in meters") ;
    args.addOption("--camera", camera_intrinsics).setName("<filename>").setDescription("Path to camera intrinsics file") ;

    if ( !args.parse(argc, (const char **)argv) || print_help ) {
        cout << "Usage: handeye_calibration [options]" << endl ;
        cout << "Options:" << endl ;
        args.printOptions(std::cout) ;
        exit(1) ;
    }

    CameraCalibration calib ;
    CameraCalibration::Data cdata ;

    if ( compute_markers ) {
        auto files = Path::entries(data_folder, matchFilesWithGlobPattern("*.png"), false) ;

        AprilTagDetector adet(AprilTag36h11, 4) ; // we need decimation othwerwise detection does not work
        AprilTagGridPattern agrid(pattern_grid, pattern_width, pattern_gap, adet) ;

        calib.detect(files, agrid, cdata) ;

        cdata.save(data_folder + "/calib.data") ;
    }
    else
        cdata.load(data_folder + "/calib.data") ;

    PinholeCamera cam ;

    if ( camera_intrinsics.empty() ) {
        cam.setSize(cdata.fsize_);
    }
    else
        cam.read(camera_intrinsics) ;

    vector<Matrix4d> extrinsics ;
//    calib.run(cdata, cam, extrinsics, cv::CALIB_USE_INTRINSIC_GUESS) ;


  //  calib.refine(cdata, cam, extrinsics, false) ;
    vector<Affine3d> gripper_to_base, target_to_sensor ;

    find_target_motions(cam, cdata, extrinsics, gripper_to_base, target_to_sensor) ;

//    gripper_to_base.resize(50) ;
 //   target_to_sensor.resize(50) ;

    Affine3d sensor_to_base ;
    HandEyeCalibration::Parameters heyeparams ;
    heyeparams.method_ = HandEyeCalibration::DualQuaternion;
    heyeparams.refine_ = true ;
    HandEyeCalibration heyecal(heyeparams) ;
    heyecal.solveFixed(gripper_to_base, target_to_sensor, sensor_to_base) ;

    cout << sensor_to_base.matrix() ;

    print_gripper_locations(cam, cdata, gripper_to_base, sensor_to_base);

    return 1;
}
