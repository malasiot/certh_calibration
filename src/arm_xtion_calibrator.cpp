#include <ros/ros.h>
#include <camera_helpers/openni_capture.hpp>
#include <viz_helpers/camera_view_client.hpp>
#include <robot_helpers/robot.hpp>
#include <robot_helpers/geometry.hpp>
#include <visualization_msgs/Marker.h>

#include <fstream>


#include "xtionCalibrator.h"

using namespace std ;
using namespace Eigen ;
using namespace robot_helpers ;

string camera = "xtion2", armName, arm2Name ;
bool calibration_finished ;
const Eigen::Vector3d arm2Point (1,      -1.3 ,    1), arm1Point (-1,      -1.3 ,    1) ;
const Eigen::Vector3d arm1StartingPoint (-0.5, -1 ,1), arm2StartingPoint (0.5, -1 ,1) ;
int phase ;
int nPoints ;
int currPoint ;
float step ;
std::vector < Eigen::Vector3d > relativePoints;
xtionCalibrator calib;
Eigen::Vector3d xtionYVec, xtionZVec;

struct Context {

    int currentPoint ;
    vector<pcl::PointXYZ> pts ;
    vector<Vector3d> rpts ;
    robot_helpers::Robot2 *robot ;
    viz_helpers::CameraViewServer *gui ;
    string outFolder, camera ;
    bool fixed ;

    std::vector <Eigen::Vector3d> xtionDepthPoints ;
    std::vector <Eigen::Vector3d> armPoints ;
    int maxPoints  ;
};

ofstream myfile;

void getXY(int& x, int& y, cv::Mat depth){

    bool found = false;
    int col,row;
    for(col =x; col<depth.cols; ++col){
        for(row = y; row<depth.rows; row++){
            unsigned short d = depth.at<unsigned short>(row,col);
            if(d != 0 && d < 2100){
                found = true;
                break;
            }
        }
        if(found)
            break;
    }
    x = col;
    y = row;


}

void onMouseClicked(int x, int y, Context *ctx)
{

    image_geometry::PinholeCameraModel cm ;
    cv::Mat clr, depth ;
    ros::Time ts ;


    camera_helpers::openni::grab(camera, clr, depth, ts, cm) ;

    getXY(x,y,depth);
    std::cout << x << " " << y << " " << depth.at<unsigned short>(y,x) <<  std::endl;

    ctx->xtionDepthPoints.push_back(Eigen::Vector3d(x, y,(double) depth.at<unsigned short>(y, x)/1000.0));
    ctx->armPoints.push_back( getTransformationAffine(arm2Name + "_ee", armName + "_link_6").translation());

    if(myfile.is_open()){
        myfile << x << " " << y << " " << (double) depth.at<unsigned short>(y, x) << endl;
    }

    Eigen::Matrix4d link6calib = getTransformationMatrix(armName + "_link_6"  );
    Eigen::Vector3d armPose =  getTransformationAffine(arm2Name + "_ee" ,armName + "_link_6"  ).translation();
     Eigen::Vector4d armPose4D ;
    if(phase == 0  ){

        if(currPoint < nPoints){
            currPoint++ ;


            if(armName == "r1"){
                armPose.z() -=  step ;
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r2");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0), M_PI) ) ;

            }
            else{
                armPose.z() -=  step ;
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r1");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0)) ) ;
            }


        }
        else{

            calib.calcFx(ctx->xtionDepthPoints, ctx->armPoints) ;
            calib.writeIntrinsics("/tmp/data.txt") ;
            ctx->robot->getCommander()->setPoseReferenceFrame( "torso_link");
            if(armName == "r2"){
                 ctx->robot->setCurrentGroup(arm2Name);
                 ctx->robot->moveGripperToPoseIK( arm1Point, lookAt(Vector3d(0,-1, 0), M_PI)) ;
            }
            else{
                ctx->robot->setCurrentGroup(arm2Name);
                ctx->robot->moveGripperToPoseIK( arm2Point, lookAt(Vector3d(0,-1, 0), -M_PI)) ;
            }
            currPoint = 0 ;
            phase ++ ;
            ctx->xtionDepthPoints.clear() ;
            ctx->armPoints.clear() ;

        }


    }

    else if(phase == 1  ){

        if(currPoint < nPoints){
            currPoint++ ;


            if(armName == "r1"){
                armPose.y() +=  step ;
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r2");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0), M_PI) ) ;

            }
            else{
                armPose.y() +=  step ;
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r1");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0)) ) ;
            }


        }
        else{

            xtionYVec = calib.calcYVec(ctx->xtionDepthPoints, ctx->armPoints);
            std::cout << xtionYVec << std::endl;
            xtionYVec << -xtionYVec(0), -xtionYVec(2), 0;
            xtionZVec = Eigen::Vector3d(0,0,1).cross(xtionYVec);
            calib.writeIntrinsics("/tmp/data.txt") ;
            ctx->robot->getCommander()->setPoseReferenceFrame( "torso_link");
            if(armName == "r2"){
                 ctx->robot->setCurrentGroup(arm2Name);
                 ctx->robot->moveGripperToPoseIK( arm1Point, lookAt(Vector3d(0,-1, 0), M_PI)) ;
            }
            else{
                ctx->robot->setCurrentGroup(arm2Name);
                ctx->robot->moveGripperToPoseIK( arm2Point, lookAt(Vector3d(0,-1, 0), -M_PI)) ;
            }
            currPoint = 0 ;
            phase ++ ;
            ctx->xtionDepthPoints.clear() ;
            ctx->armPoints.clear() ;
        }


    }

    else if(phase == 2 ){

        if(currPoint < nPoints){
            currPoint++ ;


            if(armName == "r1"){
                //armPose.z() += step * xtionYVec(0);
                armPose.y() += step * xtionYVec(1);
                armPose.x() += step * xtionYVec(0);
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r2");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0), M_PI) ) ;

            }
            else{
                armPose.x() += step * xtionYVec(0);
                armPose.y() += step * xtionYVec(1);
                armPose.z() += step * xtionYVec(2);
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r1");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0)) ) ;
            }


        }
        else{

            calib.calcFy(ctx->xtionDepthPoints, ctx->armPoints);
            calib.writeIntrinsics("/tmp/data.txt") ;
            ctx->robot->getCommander()->setPoseReferenceFrame( "torso_link");
            if(armName == "r2"){
                 ctx->robot->setCurrentGroup(arm2Name);
                 ctx->robot->moveGripperToPoseIK( arm1Point, lookAt(Vector3d(0,-1, 0), M_PI)) ;
            }
            else{
                ctx->robot->setCurrentGroup(arm2Name);
                ctx->robot->moveGripperToPoseIK( arm2Point, lookAt(Vector3d(0,-1, 0), -M_PI)) ;
            }
            currPoint = 0 ;
            phase ++ ;
            ctx->xtionDepthPoints.clear() ;
            ctx->armPoints.clear() ;

        }


    }

    else if(phase == 3 ){

        if(currPoint < nPoints){
            currPoint++ ;


            if(armName == "r1"){


                armPose.x() += step * (xtionYVec(0) + xtionZVec(0));
                armPose.y() += step * (xtionYVec(1) + xtionZVec(1));
                armPose.z() += step;
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r2");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0), M_PI) ) ;

            }
            else{
                armPose.x() += step * xtionYVec(0);
                armPose.y() += step * xtionYVec(1);
                armPose.z() += step * xtionYVec(2);
                armPose4D  << armPose.x(), armPose.y(), armPose.z(), 1 ;
                ctx->robot->setCurrentGroup("r1");
                move_group_interface::MoveGroup::Plan traj;
                Eigen::Vector4d newPose = link6calib*armPose4D ;
                armPose << newPose.x(), newPose.y(), newPose.z() ;
                moveArm(*(ctx->robot),armPose , lookAt(Eigen::Vector3d(0,-1, 0)) ) ;
            }


        }
        else{

            calib.calcCxy(ctx->xtionDepthPoints, step);
            calib.writeIntrinsics("/tmp/data.txt") ;
            ctx->robot->getCommander()->setPoseReferenceFrame( "torso_link");
            if(armName == "r2"){
                 ctx->robot->setCurrentGroup(arm2Name);
                 ctx->robot->moveGripperToPoseIK( arm1Point, lookAt(Vector3d(0,-1, 0), M_PI)) ;
            }
            else{
                ctx->robot->setCurrentGroup(arm2Name);
                ctx->robot->moveGripperToPoseIK( arm2Point, lookAt(Vector3d(0,-1, 0), -M_PI)) ;
            }
            currPoint = 0 ;
            phase ++ ;
            ctx->xtionDepthPoints.clear() ;
            ctx->armPoints.clear() ;
            calibration_finished = true ;
        }


    }






//    if ( ctx->currentPoint == ctx->maxPoints )
//    {
//        // all points have been added, do the calibration
//        myfile.close();

//        xtionCalibrator calib;
//        calib.setData(ctx->xtionDepthPoints,ctx->xtionRotMats );
//        calib.calibrateIntrinsics();
//        calib.writeIntrinsics("test.");


//        calibration_finished = true ;

//        ctx->gui->sendMessage("Calibration finished") ;
//        return ;
//    }
//    else
//    {
//        if(armName == "r1"){
//            ctx->robot->setCurrentGroup("r1");
//            move_group_interface::MoveGroup::Plan traj;
//            Eigen::Vector3d newPose = arm2Point + relativePoints[ ctx->currentPoint] ;
//            if(ctx->robot->planXtionToPose(arm1StartingPoint , lookAtPoint(arm1StartingPoint, newPose ), traj) )
//                ctx->robot->execute(traj) ;
//        }
//        else{
//            ctx->robot->setCurrentGroup("r2");
//            move_group_interface::MoveGroup::Plan traj;
//            Eigen::Vector3d newPose = arm1Point + relativePoints[ ctx->currentPoint] ;
//            if(ctx->robot->planXtionToPose(arm2StartingPoint , lookAtPoint(arm2StartingPoint, newPose ), traj) )
//                ctx->robot->execute(traj) ;
//        }

//    }

//    ctx->currentPoint++ ;
}

void createRelativePointMatrix(float dx, float dz){

    relativePoints.push_back( Eigen::Vector3d (0, -dx , -dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, dx , dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, -dx , dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, dx , -dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, 0 , -dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0,  0 , dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, -dx , 0)) ;
    relativePoints.push_back( Eigen::Vector3d (0, dx , 0)) ;

}


int main(int argc, char **argv) {

    ros::init(argc, argv, "calibrate_arm_xtion");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    int c = 1 ;

    while ( c < argc )
    {

        if ( strncmp(argv[c], "--camera", 8) == 0 )
        {
            if ( c + 1 < argc ) {
                camera = argv[++c] ;
            }
        }

        ++c ;
    }

     if(camera == "xtion2" ) {
         armName = "r2" ;
         arm2Name = "r1" ;
     }
     else{
         armName = "r1" ;
         arm2Name = "r2" ;

     }

     createRelativePointMatrix(0.4, 0.4);

     string outFolder ;
     viz_helpers::CameraViewServer srv ;

     Robot2 rb("arms");
   //  rb.moveHomeArms();
     Context ctx ;
     ctx.currentPoint = 0 ;
     ctx.robot = &rb ;
     ctx.gui = &srv ;
     ctx.maxPoints = 8 ;
     ctx.fixed = true ;

     if ( outFolder.empty() )
     nh.getParam("/calibration_folder", ctx.outFolder) ;

     Eigen::Quaterniond q ;
     q = Eigen::AngleAxisd(-M_PI/2, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(-M_PI/2, Eigen::Vector3d::UnitZ());

      // move the robot to the first position

     move_group_interface::MoveGroup::Plan traj;

     if(armName == "r2"){

         rb.setCurrentGroup(arm2Name);
         if ( ctx.fixed )
             rb.moveGripperToPoseIK( arm1Point, lookAt(Vector3d(0,-1, 0), M_PI)) ;
         else
             rb.moveGripperToPoseIK( Eigen::Vector3d(0.0, -1.0, 1.0), q) ;

         rb.setCurrentGroup(armName);

         if(rb.planXtionToPose( arm2StartingPoint, lookAtPoint(arm2StartingPoint, arm1Point, 0), traj) ) ;
             rb.execute(traj) ;
     }
     else{

         rb.setCurrentGroup(arm2Name);
         if ( ctx.fixed )
             rb.moveGripperToPoseIK( arm2Point, lookAt(Vector3d(0,-1, 0), -M_PI)) ;
         else
             rb.moveGripperToPoseIK( Eigen::Vector3d(0.0, -1.0, 1.0), q) ;

         rb.setCurrentGroup(armName);

         if(rb.planXtionToPose( arm1StartingPoint, lookAtPoint(arm1StartingPoint, arm2Point, 0), traj)) ;
             rb.execute(traj) ;
     }

     phase = 0 ;
     nPoints = 4 ;
     currPoint = 0 ;
     step = 0.1 ;
     myfile.open("/tmp/data.txt");
     srv.mouseClicked.connect(boost::bind(onMouseClicked, _1, _2, &ctx));

     srv.sendMessage("Click on the image to select gripper tip point") ;

     // wait until calibration has finished

     while (!calibration_finished) ;


}
