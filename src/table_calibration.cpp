#include <ros/ros.h>

#include <camera_helpers/OpenNIServiceClient.h>
#include <camera_helpers/Util.h>
#include <robot_helpers/Robot.h>
#include <robot_helpers/Geometry.h>
#include <certh_libs/Helpers.h>
#include <boost/filesystem.hpp>
#include <viz_helpers/CameraViewServer.h>

#include <fstream>

#include <highgui.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

using namespace std ;
using namespace Eigen ;
using namespace robot_helpers ;


class TableCalibration {
public:
    TableCalibration(Robot &rb, const string &table_id, const cv::Mat &rgb,
                     const Affine3d &cam_frame, const image_geometry::PinholeCameraModel &cm, const string &out_folder):
        rb_(rb), table_id_(table_id), orig_(rgb), it_(ros::NodeHandle("~")),
        cam_frame_(cam_frame), cm_(cm), outFolder_(out_folder)
    {

        image_transport::SubscriberStatusCallback cb =
                boost::bind(&TableCalibration::clientConnectCb, this, _1) ;

        pub_ = it_.advertise("image", 1, cb );

        getTableGeometry(tframe, twidth, theight) ;

        feedback_ = orig_.clone() ;
        projectTablePoints(feedback_, tframe, twidth, theight, cam_frame_, cm) ;

        srv_.mouseClicked.connect(boost::bind(&TableCalibration::onMouseClicked, this, _1, _2));
        srv_.keyPressed.connect(boost::bind(&TableCalibration::onKeyPressed, this, _1));

        srv_.sendMessage("Click on the table corners. Press S to save the calibration.") ;

    }

    void run() ;

private:

    void onMouseClicked(int x, int y) ;
    void onKeyPressed(const string &key) ;
    void getTableGeometry(Affine3d &frame, double &width, double &height) ;

    void projectTablePoints(cv::Mat &src, const Affine3d &frame, double width, double height, const Affine3d &cam_frame, const image_geometry::PinholeCameraModel &cm) ;
    void clientConnectCb(const image_transport::SingleSubscriberPublisher&) ;
    void doCalibrate() ;

    Robot &rb_ ;
    string table_id_ ;
    cv::Mat orig_, feedback_ ;
    image_transport::ImageTransport it_;

    image_transport::Publisher pub_ ;
    Affine3d cam_frame_ ;
    image_geometry::PinholeCameraModel cm_ ;
    viz_helpers::CameraViewServer srv_ ;

    Affine3d tframe ;
    double twidth, theight ;
    int np ;
    vector<cv::Point> pts ;
    vector<cv::Point2d> ppts ;
    string outFolder_ ;
    bool finished ;

};

void TableCalibration::getTableGeometry(Affine3d &frame, double &width, double &height)
{
    Affine3d leg_1 = rb_.getTransform(table_id_ + "_leg_1") ;
    Affine3d leg_2 = rb_.getTransform(table_id_ + "_leg_2") ;
    Affine3d leg_3 = rb_.getTransform(table_id_ + "_leg_3") ;

    frame = rb_.getTransform(table_id_ + "_desk") ;

    width = ((frame * leg_1.inverse()).translation() - (frame*leg_2.inverse()).translation()).norm() ;
    height = ((frame * leg_2.inverse()).translation() - (frame*leg_3.inverse()).translation()).norm() ;

    // corners in the world frame

    Vector3d p1 = frame.inverse() * Vector3d(-width/2, height/2, 0) ;
    Vector3d p2 = frame.inverse() * Vector3d( width/2, height/2, 0) ;
    Vector3d p3 = frame.inverse() * Vector3d( width/2, -height/2, 0) ;
    Vector3d p4 = frame.inverse() * Vector3d(-width/2, -height/2, 0) ;

    //cout << p1 << endl << p2 << endl << p3 << endl << p4 << endl ;
}

void TableCalibration::clientConnectCb(const image_transport::SingleSubscriberPublisher &)
{
    if ( pub_.getNumSubscribers() > 0 )
    {
        sensor_msgs::ImagePtr img = camera_helpers::cvImageToMsg(feedback_) ;
        pub_.publish(img) ;
    }

}

void TableCalibration::onKeyPressed(const string &key)
{
    cout << key << endl ;

    if ( key == "S" )
    {
        ofstream ostrm(outFolder_.c_str()) ;

        ostrm << 2 << endl ;
        ostrm << table_id_ + "_desk" << endl ;
        ostrm << tframe.matrix() ;

        finished = true ;
    }


}

void TableCalibration::run()
{
    finished = false ;

    while ( !finished )
    {
        ros::Duration(0.5).sleep() ;
    }
}

void TableCalibration::onMouseClicked(int x, int y)
{
    pts.push_back(cv::Point(x, y)) ;

    if ( pts.size() == 4 )
        doCalibrate() ;

    feedback_ = orig_.clone() ;

    projectTablePoints(feedback_, tframe, twidth, theight, cam_frame_, cm_);

    sensor_msgs::ImagePtr img = camera_helpers::cvImageToMsg(feedback_) ;
    pub_.publish(img) ;

}

void TableCalibration::doCalibrate()
{
    cv::Mat cameraMatrix = cv::Mat::eye(3, 3, CV_64F);

    cameraMatrix.at<double>(0, 0) = cm_.fx() ;
    cameraMatrix.at<double>(1, 1) = cm_.fy() ;
    cameraMatrix.at<double>(0, 2) = cm_.cx() ;
    cameraMatrix.at<double>(1, 2) = cm_.cy() ;

    vector<cv::Vec3f> objectPoints ;

    objectPoints.push_back(cv::Vec3f(-twidth/2, theight/2, 0)) ;
    objectPoints.push_back(cv::Vec3f( twidth/2, theight/2, 0)) ;
    objectPoints.push_back(cv::Vec3f( twidth/2, -theight/2, 0)) ;
    objectPoints.push_back(cv::Vec3f(-twidth/2, -theight/2, 0)) ;

    vector<cv::Vec2f> imagePts ;

    for(int i=0 ; i<4 ; i++)
    {
        cv::Point2d &pp = ppts[i] ;

        double minDist = DBL_MAX ;
        int bestj ;

        for(int j=0 ; j<4 ; j++ )
        {
            double dist = (pp.x - pts[j].x) * (pp.x - pts[j].x) + (pp.y - pts[j].y) * (pp.y - pts[j].y) ;

            if ( dist < minDist ) {
                minDist = dist ;
                bestj = j ;
            }
        }

        imagePts.push_back(cv::Vec2f(pts[bestj].x, pts[bestj].y)) ;
    }

    cv::Mat distCoeffs = cv::Mat::zeros(8, 1, CV_64F), rvec, tvec;

    cv::solvePnP(objectPoints, imagePts, cameraMatrix, distCoeffs, rvec, tvec, false, CV_EPNP) ;
//    cv::solvePnP(objectPoints, imagePts, cameraMatrix, distCoeffs, rvec, tvec, true) ;

    cv::Mat rmat_ ;
    cv::Rodrigues(rvec, rmat_) ;

    cv::Mat_<double> rmat(rmat_) ;
    cv::Mat_<double> tmat(tvec) ;

    Matrix3d r ;
    Vector3d t ;

    r << rmat(0, 0), rmat(0, 1), rmat(0, 2), rmat(1, 0), rmat(1, 1), rmat(1, 2), rmat(2, 0), rmat(2, 1), rmat(2, 2) ;
    t << tmat(0, 0), tmat(0, 1), tmat(0, 2) ;

    Affine3d tr = Translation3d(t) * r ;

    tframe =  (cam_frame_ * tr).inverse() ;

    cout << tframe.matrix() << endl ;

    pts.clear() ;
}

static cv::Point2d projectPoint(const Vector3d &p, const image_geometry::PinholeCameraModel &cm)
{
   return  cm.project3dToPixel(cv::Point3d(p.x(), p.y(), p.z())) ;

}

void TableCalibration::projectTablePoints(cv::Mat &src, const Affine3d &frame, double width, double height, const Affine3d &cam_frame, const image_geometry::PinholeCameraModel &cm)
{
    Vector3d p1 = frame.inverse() * Vector3d(-width/2, height/2, 0) ;
    Vector3d p2 = frame.inverse() * Vector3d( width/2, height/2, 0) ;
    Vector3d p3 = frame.inverse() * Vector3d( width/2, -height/2, 0) ;
    Vector3d p4 = frame.inverse() * Vector3d(-width/2, -height/2, 0) ;

    cout << p1 << endl << p2 << endl << p3 << endl << p4 << endl ;

    Vector3d pc1 = cam_frame.inverse() * p1 ;
    Vector3d pc2 = cam_frame.inverse() * p2 ;
    Vector3d pc3 = cam_frame.inverse() * p3 ;
    Vector3d pc4 = cam_frame.inverse() * p4 ;

    cv::Point2d pp1 = projectPoint(pc1, cm) ;
    cv::Point2d pp2 = projectPoint(pc2, cm) ;
    cv::Point2d pp3 = projectPoint(pc3, cm) ;
    cv::Point2d pp4 = projectPoint(pc4, cm) ;

    ppts.clear() ;

    ppts.push_back(pp1) ;
    ppts.push_back(pp2) ;
    ppts.push_back(pp3) ;
    ppts.push_back(pp4) ;

    double minDist = std::min(sqrt((pp1 - pp2).dot(pp1 - pp2)), sqrt((pp2 - pp3).dot(pp2 - pp3))) ;

    cv::line(src, cv::Point(pp1.x, pp1.y), cv::Point(pp2.x, pp2.y), cv::Scalar(255, 0, 0), 2) ;
    cv::line(src, cv::Point(pp2.x, pp2.y), cv::Point(pp3.x, pp3.y), cv::Scalar(255, 0, 0), 2) ;
    cv::line(src, cv::Point(pp3.x, pp3.y), cv::Point(pp4.x, pp4.y), cv::Scalar(255, 0, 0), 2) ;
    cv::line(src, cv::Point(pp4.x, pp4.y), cv::Point(pp1.x, pp1.y), cv::Scalar(255, 0, 0), 2) ;

    for(int i=0 ; i<pts.size() ; i++ )
    {
        cv::line(src, cv::Point(pts[i].x - 3, pts[i].y), cv::Point(pts[i].x + 3, pts[i].y), cv::Scalar(0, 0, 255), 2) ;
        cv::line(src, cv::Point(pts[i].x, pts[i].y-3), cv::Point(pts[i].x, pts[i].y+3), cv::Scalar(0, 0, 255), 2) ;

    }

/*
    vector<cv::Point2f> srcPts, dstPts ;
    srcPts.push_back(cv::Point2d(pp1.x, pp1.y)) ;
    srcPts.push_back(cv::Point2d(pp2.x, pp2.y)) ;
    srcPts.push_back(cv::Point2d(pp3.x, pp3.y)) ;
    srcPts.push_back(cv::Point2d(pp4.x, pp4.y)) ;


    dstPts.push_back(cv::Point2f(0, 0)) ;
    dstPts.push_back(cv::Point2f(500, 0)) ;
    dstPts.push_back(cv::Point2f(500, 500)) ;
    dstPts.push_back(cv::Point2f(0, 500)) ;

    cv::Mat pt = cv::getPerspectiveTransform(srcPts, dstPts) ;

    cv::Mat dst ;
    cv::warpPerspective(src, dst, pt, cv::Size(500, 500)) ;

    cv::imwrite("/tmp/warped.png", dst) ;
*/


}

int main(int argc, char **argv) {

    ros::init(argc, argv, "table_calibration");
    ros::NodeHandle nh ;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    srand(clock()) ;

    string camera_id = "xtion3", armName, table_id = "t2" ;
    string outFolder ;

    double sx, sy ;
    Vector3d orient ;
    double sensorDist ;
    bool orient_given = false, sensor_dist_given = false ;
    bool fixedCam = false ;

    int c = 1 ;

    while ( c < argc )
    {
        if ( strncmp(argv[c], "--camera", 8) == 0 )
        {
            if ( c + 1 < argc ) {
                camera_id = argv[++c] ;
            }
        }
        else if ( strncmp(argv[c], "--arm", 5) == 0 )
        {
            if ( c + 1 < argc ) {
                armName = argv[++c] ;
            }
        }
        else if ( strncmp(argv[c], "--table", 7) == 0 )
        {
            if ( c + 1 < argc ) {
                table_id = argv[++c] ;
            }

        }
        else if ( strncmp(argv[c], "--out", 5) == 0 )
        {
            if ( c + 1 < argc ) {
                outFolder = argv[++c] ;
            }
        }
        else if ( strncmp(argv[c], "--orient", 8) == 0 )
        {
            double ox, oy, oz ;

            if ( c + 1 < argc ) {
                ox = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                oy = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                oz = atof(argv[++c]) ;
            }

            orient = Vector3d(ox, oy, oz) ;
            orient_given = true ;
        }
        else if ( strncmp(argv[c], "--dist", 6) == 0 )
        {

            if ( c + 1 < argc ) {
                sensorDist = atof(argv[++c]) ;
                sensor_dist_given = true ;
            }

        }
        else if ( strncmp(argv[c], "--fixed", 7) == 0 )
        {
            fixedCam = true ;

        }

        ++c ;
    }

    if ( camera_id.empty() )
    {
        ROS_ERROR("No camera specified") ;
        return 0 ;
    }

    if ( table_id.empty() )
    {
        ROS_ERROR("No table specified") ;
        return 0 ;
    }

    if ( outFolder.empty() )
    {
        string calibFolder ;

        nh.getParam("/calibration_folder", calibFolder) ;

        if ( calibFolder.empty() )
        {
            ROS_ERROR("no output path specified") ;
            exit(0) ;
        }

        outFolder = calibFolder ;
        outFolder += '/' ;
        outFolder += table_id ;
        outFolder += "_desk.calib" ;

        boost::filesystem::path p(outFolder) ;
        certh_libs::createDir(p.parent_path().string(), true) ;
    }

    if ( !fixedCam && ( !orient_given || !sensor_dist_given))
    {
        ROS_ERROR("Sensor distance/orientation from target not given") ;
        return 0 ;
    }




    Robot rb ;

    if ( !camera_helpers::openni::connect(camera_id) )
    {
        ROS_ERROR_STREAM("cannot connect to " << camera_id) ;
        return 0 ;
    }

    // start planning to position

    if ( !fixedCam )
    {
        Affine3d tframe = rb.getTransform("torso_link", table_id + "_desk") ;

        trajectory_msgs::JointTrajectory traj ;

        Vector3d pos = tframe.translation() - orient * sensorDist ;

        if ( !rb.planXtionToPose(armName, pos, lookAt(orient), traj) || !rb.execute(traj))
        {
            ROS_ERROR("Cannot move above table. Planning failed.") ;
            return 0 ;
        }

    }


    cv::Mat clr, depth ;
    ros::Time ts ;
    image_geometry::PinholeCameraModel cm ;

    camera_helpers::openni::grab(camera_id, clr, depth, ts, cm) ;

    cv::Mat rgb ;
    rgb = cv::imread("/home/malasiot/images/clothes/table cap/table cap 1/rgb.png") ;

    fstream strm("/home/malasiot/images/clothes/table cap/table cap 1/pose.txt") ;


    double val[16] ;
    Matrix4d cf ;

    for(int i=0 ; i<16 ; i++ )
    {
        strm >> val[i] ;
    }

    cf << val[0], val[1], val[2], val[3],
          val[4], val[5], val[6], val[7],
          val[8], val[9], val[10], val[11],
          val[12], val[13], val[14], val[15] ;

    Affine3d cam_frame = rb.getTransform(camera_id + "_rgb_optical_frame") ;

    camera_helpers::openni::disconnect(camera_id) ;

    TableCalibration calib(rb, table_id, rgb, Affine3d(cf), cm, outFolder) ;
    //    TableCalibration calib(rb, table_id, clr, cam_frame, cm, outFolder) ;
    calib.run() ;


    return 1;
}
