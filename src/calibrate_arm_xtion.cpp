
#include <ros/ros.h>

#include <camera_helpers/OpenNIServiceClient.h>
#include <viz_helpers/CameraViewServer.h>
#include <robot_helpers/Robot2.h>
#include <robot_helpers/Utils2.h>
#include <robot_helpers/Geometry.h>
#include <visualization_msgs/Marker.h>
#include <certh_libs/Helpers.h>

#include <fstream>
#include <highgui.h>
#include <pcl/io/pcd_io.h>

using namespace std ;
using namespace Eigen ;
using namespace robot_helpers ;

bool calibration_finished ;

float pointsR2_[18][3] = {
-1,      -1 ,    1,
-1.2,     -1,   1.1,
-1,  	-0.7,   1,
-0.8, 	-1,     0.7,
-1,   -1.3, 	1,
-1.2,  	-0.7, 	1,
-1,      -1 ,    1,
-1.2,      -1 ,    1,
-0.8,      -1 ,    1

} ;

float pointsR1_[30][3] = {
1,      -1 ,    1,
1.2,     -1,   1.1,
1,  	-0.7,   1,
0.8, 	-1,     0.7,
1,   -1.3, 	1,
1.2,  	-0.7, 	1,
1,      -1 ,    1,
1.2,      -1 ,    1,
0.8,      -1 ,    1,

1.1,      -1 ,    1.1,
1.2,     -1,   1.2,
0.9,  	-0.8,   1,
0.8, 	-1,     1,
0.7,   -1.1, 	1,
1.1,  	-0.9, 	1.3,
0.9,      -1.2 ,    1,
1.3,      -0.8 ,    0.7,
0.9,      -1.1 ,    1.8

} ;

 double minX = -0.25 ;
 double maxX = 0.25 ;
 double minY = -1.3 ;
 double maxY = -0.8 ;
 double minZ = 0.9 ;
 double maxZ = 1.5 ;


int maxPoints = 30 ;

std::string camera = "xtion2" ;
std::string armName ;
std::string arm2Name ;


void doCalibrate(const vector<pcl::PointXYZ> &pts, const vector<Vector3d> &points, const string &outFolder)
{
    cv::Mat meanRobot = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));

    int n = pts.size() ;

    for (unsigned int i=0;i<n;i++){
        for (unsigned int j=0;j<3;j++){
            meanRobot.at<float>(j, 0) += (points[i][j]/(float)n);
        }
    }

    cv::Mat xtionPoints(n, 3, CV_32FC1, cv::Scalar::all(0));

    cv::Mat meanXtion = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));

    for (unsigned int i=0;i<n;i++)
    {
        const pcl::PointXYZ &p = pts[i] ;

        cout << p.x << ' ' << p.y << ' ' << p.z << endl ;

        xtionPoints.at<float>(i, 0) = p.x ;
        xtionPoints.at<float>(i, 1) = p.y ;
        xtionPoints.at<float>(i, 2) = p.z ;

        //cout << "Xtion points: " << p.x << " " << p.y << " " << p.z << std::endl;

        meanXtion.at<float>(0, 0) += p.x/n;
        meanXtion.at<float>(1, 0) += p.y/n;
        meanXtion.at<float>(2, 0) += p.z/n;

        cout << points[i] << endl ;
    }


    cv::Mat H = cv::Mat(3, 3, CV_32FC1, cv::Scalar::all(0));
    cv::Mat Pa = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));
    cv::Mat Pb = cv::Mat(1, 3, CV_32FC1, cv::Scalar::all(0));

    for(int i=0; i<n; ++i){
        for(int j=0; j<3; ++j){
            Pa.at<float>(j, 0) = xtionPoints.at<float>(i, j) - meanXtion.at<float>(j, 0);
            Pb.at<float>(0, j) = points[i][j] - meanRobot.at<float>(j, 0);
        }
        H += Pa * Pb;
    }

    cv::SVD svd(H, cv::SVD::FULL_UV);
    cv::Mat tr(4, 4, CV_32FC1, cv::Scalar::all(0)) ;
    cv::Mat V = svd.vt.t();
    double det = cv::determinant(V);

    if(det < 0){
        for(int i=0; i<V.rows; ++i)
            V.at<float>(i,3) *= -1;
    }

    cv::Mat R = V * svd.u.t();
    cv::Mat t = (-1)*R*meanXtion + meanRobot;
    cout << meanXtion << ' ' << meanRobot << endl ;
    cout << R << t << endl ;
    cout << H << endl ;
    for(int i=0; i<3; ++i)
        for(int j=0; j<3; ++j)
            tr.at<float>(i, j) = R.at<float>(i, j) ;

    for(int i=0 ; i<3 ; i++)
        tr.at<float>(i, 3) = t.at<float>(i, 0) ;

    tr.at<float>(3, 3) = 1.0 ;

    cv::Mat trInv = tr.inv() ;
    string outPath ;

    if ( outFolder.empty() )
    {
        outPath = getenv("HOME") ;
        outPath += "/.ros/calibration/" + armName + "_handeye.calib" ;
    }
    else
        outPath = outFolder + "/" + camera + "_handeye.calib" ;
    cout << "calibration path  = " << outPath << endl;
    boost::filesystem::path p(outPath) ;
    certh_libs::createDir(p.parent_path().string(), true) ;

    ofstream fout(outPath.c_str());

    fout << 2 << endl ;
    fout <<  camera + "_link_ee" << endl ;

    for(int i=0; i<4; ++i) {
        for(int j=0; j<4; ++j)
            fout << trInv.at<float>(i, j) << " " ;

        fout << endl;
    }

    cout << trInv << endl;
}

struct Context {
    int currentPoint ;

    vector<pcl::PointXYZ> pts ;
    vector<Vector3d> rpts ;
    robot_helpers::Robot2 *robot ;
    viz_helpers::CameraViewServer *gui ;
    string outFolder, camera ;
    bool fixed ;
};


void getXY(int& x, int& y, cv::Mat depth){

    bool found = false;
    int col,row;
    for(col =x-20; col<x+20; ++col){
        for(row = y-20; row<y+20; row++){
            unsigned short d = depth.at<unsigned short>(row,col);
            if(d != 0 && d < 2100){
                found = true;
                break;
            }
        }
        if(found)
            break;
    }
    x = col;
    y = row;


}

int counter = 0 ;

void onMouseClicked(int x, int y, Context *ctx)
{
    //cout << x << ' ' << y << endl ;

    // capture point cloud

    pcl::PointCloud<pcl::PointXYZ> cloud ;
    image_geometry::PinholeCameraModel cm ;
    cv::Mat clr, depth ;

    ros::Time ts ;

    camera_helpers::openni::grab(camera, clr, depth, cloud, ts, cm) ;

    getXY(x, y , depth) ;

/*
    string filenamePrefix = str(boost::format("/tmp/grab_%06d") % counter++) ;

    cv::imwrite(filenamePrefix + "_c.png", clr) ;
    cv::imwrite(filenamePrefix + "_d.png", depth) ;

    pcl::io::savePCDFileBinary(filenamePrefix + ".pcd", cloud) ;

    Eigen::Affine3d pose_ = ctx->robot->getGripperPose("r2") ;

    {
        ofstream strm((filenamePrefix + "_pose.txt").c_str()) ;

        strm << pose_.rotation() << endl << pose_.translation() ;
    }
*/
    // get the 3D coordinate on clicked position

    pcl::PointXYZ p = cloud.at(x, y);

    cout << x << ' ' << y << ' ' << p.x << ' ' << p.y << ' ' << ' ' << p.z << endl ;

    Vector3d rp = ctx->robot->getTransform(armName + "_link_6", arm2Name + "_ee", ts).translation() ;

    // add the point to the list

    ctx->pts.push_back(p) ;
    ctx->rpts.push_back(rp) ;

    ctx->currentPoint++ ;

    int nPts = ( ctx->fixed ) ? 17 : maxPoints ;

    if ( ctx->currentPoint == nPts )
    {
        // all points have been added, do the calibration

        doCalibrate(ctx->pts, ctx->rpts, ctx->outFolder) ;

        calibration_finished = true ;

        ctx->gui->sendMessage("Calibration finished") ;
        return ;
    }
    else
    {
        // move the robot to the next position


        int c = ctx->currentPoint ;

        ctx->gui->sendMessage("Moving the robot to the next position") ;
        ctx->robot->setCurrentGroup(arm2Name);
        if ( ctx->fixed )
            if(armName=="r2")
                ctx->robot->moveGripperToPose( Eigen::Vector3d(pointsR2_[c][0], pointsR2_[c][1], pointsR2_[c][2]), lookAt(Vector3d(0, -1, 0), M_PI))  ;
            else
                ctx->robot->moveGripperToPose( Eigen::Vector3d(pointsR1_[c][0], pointsR1_[c][1], pointsR1_[c][2]), lookAt(Vector3d(0, -1, 0), M_PI))  ;
        else
        {
            while (1)
            {
                double X = minX + (maxX - minX)*(rand()/(double)RAND_MAX) ;
                double Y = minY + (maxY - minY)*(rand()/(double)RAND_MAX) ;
                double Z = minZ + (maxZ - minZ)*(rand()/(double)RAND_MAX) ;

                const double qscale = 0.3 ;
                double qx = qscale * (rand()/(double)RAND_MAX - 0.5) ;
                double qy = qscale * (rand()/(double)RAND_MAX - 0.5) ;
                double qz = qscale * (rand()/(double)RAND_MAX - 0.5) ;
                double qw = qscale * (rand()/(double)RAND_MAX - 0.5) ;

                Quaterniond q = robot_helpers::lookAt(Vector3d(-1, 0, 0), M_PI) ;

//            q = Quaterniond(q.x() + qx, q.y() + qy, q.z() + qz, q.w() + qw) ;
//            q.normalize();

                if ( ctx->robot->moveGripperToPose(  Eigen::Vector3d(X, Y, Z), q) ) break ;

            }
        }

        ctx->gui->sendMessage(str(boost::format("click on image to select the gripper tip point: %d of %d") % (ctx->currentPoint+1) % nPts)) ;
    }

}


int main(int argc, char **argv) {

    ros::init(argc, argv, "calibrate_fixed_xtion");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    int c = 1 ;

    string outFolder ;

    while ( c < argc )
    {

        if ( strncmp(argv[c], "--out", 5) == 0 )
        {
            if ( c + 1 < argc ) {
                outFolder = argv[++c] ;
            }
        }
        else if ( strncmp(argv[c], "--stations", 10) == 0 )
        {
            if ( c + 1 < argc ) {
                maxPoints = atoi(argv[++c]) ;
            }
        }
        else if ( strncmp(argv[c], "--bbox", 6) == 0 )
        {
            if ( c + 1 < argc ) {
                minX = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                minY = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                minZ = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                maxX = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                maxY = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                maxZ = atof(argv[++c]) ;
            }

        }
        else if ( strncmp(argv[c], "--camera", 8) == 0 )
        {
            if ( c + 1 < argc ) {
                camera = argv[++c] ;
            }
        }

        ++c ;
    }

     if(camera == "xtion2" ) {
         armName = "r2" ;
         arm2Name = "r1" ;
     }
     else{
         armName = "r1" ;
         arm2Name = "r2" ;

     }

    srand(clock()) ;

    //ros::Publisher vis_pub = nh.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );

    viz_helpers::CameraViewServer srv ;

    // open grabber and wait until connection

    if ( ! camera_helpers::openni::connect(camera) )
    {
        ROS_ERROR("can't connect to openni camera") ;
        exit(1) ;
    }

    Robot2 rb("arms");
    rb.moveHomeArms();
    Context ctx ;
    ctx.currentPoint = 0 ;
    ctx.robot = &rb ;
    ctx.gui = &srv ;
    ctx.fixed = true ;

    if ( outFolder.empty() )
    nh.getParam("/calibration_folder", ctx.outFolder) ;

    Eigen::Quaterniond q ;
    q = Eigen::AngleAxisd(-M_PI/2, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(-M_PI/2, Eigen::Vector3d::UnitZ());

     // move the robot to the first position

    move_group_interface::MoveGroup::Plan traj;
    if(armName == "r2"){
        rb.setCurrentGroup(armName);
         if(rb.planXtionToPose( Eigen::Vector3d(0.5, -1 ,1),lookAt(Eigen::Vector3d(-1, 0 ,0)), traj )) ;
            rb.execute(traj) ;
        rb.setCurrentGroup(arm2Name);

        if ( ctx.fixed )
            rb.moveGripperToPose( Vector3d(pointsR2_[0][0], pointsR2_[0][1], pointsR2_[0][2] ), lookAt(Vector3d(0,-1, 0), M_PI)) ;
        else
            rb.moveGripperToPose( Eigen::Vector3d(0.0, -1.0, 1.0), q) ;
    }
    else{
        rb.setCurrentGroup(armName);
         if(rb.planXtionToPose( Eigen::Vector3d(-0.5, -1 ,1),lookAt(Eigen::Vector3d(1, 0 ,0)), traj )) ;
            rb.execute(traj) ;
        rb.setCurrentGroup(arm2Name);

        if ( ctx.fixed )
            rb.moveGripperToPose( Vector3d(pointsR1_[0][0], pointsR1_[0][1], pointsR1_[0][2] ), lookAt(Vector3d(0,-1, 0), -M_PI)) ;
        else
            rb.moveGripperToPose( Eigen::Vector3d(0.0, -1.0, 1.0), q) ;

    }


    // enable manual tip detection

    srv.mouseClicked.connect(boost::bind(onMouseClicked, _1, _2, &ctx));

    srv.sendMessage("Click on the image to select gripper tip point") ;

    // wait until calibration has finished

    while (!calibration_finished) ;

    rb.setServoPowerOff() ;

    camera_helpers::openni::disconnect(camera) ;
    ros::shutdown();
    return 1;
}
