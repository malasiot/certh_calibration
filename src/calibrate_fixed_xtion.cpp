#include <ros/ros.h>

#include <camera_helpers/openni_capture.hpp>
#include <viz_helpers/camera_view_client.hpp>
#include <robot_helpers/robot.hpp>
#include <robot_helpers/geometry.hpp>

#include <cvx/util/imgproc/rgbd.hpp>
#include <cvx/util/misc/path.hpp>

#include <atomic>

using namespace std ;
using namespace robot_helpers ;
using namespace cvx::util ;
using namespace std::placeholders;

std::atomic<bool> calibration_finished ;

float points_[9][3] = {
    -0.2, 	-0.9,   1.4,
    -0.0,   -0.9,   1.4,
    0.2,  	-0.9,   1.4,
    -0.2, 	-1,     1.1,
    -0.0,   -1, 	1.1,
    0.2,  	-1, 	1.1,
    -0.2, 	-1.1, 	0.8,
    -0.0,   -1.1, 	0.8,
    0.2,  	-1.1, 	0.8
} ;

double minX = -0.25 ;
double maxX = 0.25 ;
double minY = -1.3 ;
double maxY = -0.8 ;
double minZ = 0.9 ;
double maxZ = 1.5 ;


int maxPoints = 30 ;

void doCalibrate(const vector<cv::Point3d> &pts, const vector<Vector3d> &points, const string &out_folder, const string &cam)
{
    cv::Mat meanRobot = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));

    int n = pts.size() ;

    for (unsigned int i=0;i<n;i++){
        for (unsigned int j=0;j<3;j++){
            meanRobot.at<float>(j, 0) += (points[i][j]/(float)n);
        }
    }

    cv::Mat xtionPoints(n, 3, CV_32FC1, cv::Scalar::all(0));

    cv::Mat meanXtion = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));

    for (unsigned int i=0;i<n;i++)
    {
        const cv::Point3d &p = pts[i] ;

        cout << p.x << ' ' << p.y << ' ' << p.z << endl ;

        xtionPoints.at<float>(i, 0) = p.x ;
        xtionPoints.at<float>(i, 1) = p.y ;
        xtionPoints.at<float>(i, 2) = p.z ;

        //cout << "Xtion points: " << p.x << " " << p.y << " " << p.z << std::endl;

        meanXtion.at<float>(0, 0) += p.x/n;
        meanXtion.at<float>(1, 0) += p.y/n;
        meanXtion.at<float>(2, 0) += p.z/n;

        cout << points[i] << endl ;
    }


    cv::Mat H = cv::Mat(3, 3, CV_32FC1, cv::Scalar::all(0));
    cv::Mat Pa = cv::Mat(3, 1, CV_32FC1, cv::Scalar::all(0));
    cv::Mat Pb = cv::Mat(1, 3, CV_32FC1, cv::Scalar::all(0));

    for(int i=0; i<n; ++i){
        for(int j=0; j<3; ++j){
            Pa.at<float>(j, 0) = xtionPoints.at<float>(i, j) - meanXtion.at<float>(j, 0);
            Pb.at<float>(0, j) = points[i][j] - meanRobot.at<float>(j, 0);
        }
        H += Pa * Pb;
    }

    cv::SVD svd(H, cv::SVD::FULL_UV);
    cv::Mat tr(4, 4, CV_32FC1, cv::Scalar::all(0)) ;
    cv::Mat V = svd.vt.t();
    double det = cv::determinant(V);

    if(det < 0){
        for(int i=0; i<V.rows; ++i)
            V.at<float>(i,3) *= -1;
    }

    cv::Mat R = V * svd.u.t();
    cv::Mat t = (-1)*R*meanXtion + meanRobot;
    cout << meanXtion << ' ' << meanRobot << endl ;
    cout << R << t << endl ;
    cout << H << endl ;
    for(int i=0; i<3; ++i)
        for(int j=0; j<3; ++j)
            tr.at<float>(i, j) = R.at<float>(i, j) ;

    for(int i=0 ; i<3 ; i++)
        tr.at<float>(i, 3) = t.at<float>(i, 0) ;

    tr.at<float>(3, 3) = 1.0 ;

    cv::Mat trInv = tr.inv() ;
    Path out_path ;
	
    if ( out_folder.empty() ) {
        out_path = Path::homePath() / ".ros/calibration/" ;
    }
    else out_path = Path(out_folder) ;

    cout << "calibration saved at = " << out_path.toString() << endl;

    Path::mkdirs(out_path.toString()) ;

    Path calibration_file_path(out_path, cam + "_calib.txt") ;

    ofstream fout(calibration_file_path.toString().c_str());

    fout << 2 << endl ;
    fout << "xtion3_link_ee" << endl ;

    for(int i=0; i<4; ++i) {
        for(int j=0; j<4; ++j)
            fout << trInv.at<float>(i, j) << " " ;

        fout << endl;
    }

    cout << trInv << endl;
}

class Calibrator {

public:

    Calibrator(const string &camera, const string &out_folder, robot_helpers::RobotArm &arm, viz_helpers::CameraViewClient &gui):
    robot_(arm), gui_(gui), camera_(camera), out_folder_(out_folder), cap_(camera) {
        gui_.setOnMouseClickedCallback(std::bind(&Calibrator::onMouseClicked, this, std::placeholders::_1, std::placeholders::_2)) ;

    }

    bool init() {
        if (!cap_.connect(ros::Duration(2.0)) ) return false ;

        // move robot to first position

        bool res ;
        if ( fixed_ )
            res = robot_.moveTipIK(Vector3d(points_[0][0], points_[0][1], points_[0][2] ), lookAt(Vector3d(-1, 0, 0), M_PI)) ;
        else {
            Eigen::Quaterniond q ;
            q = Eigen::AngleAxisd(-M_PI/2, Eigen::Vector3d::UnitY()) * Eigen::AngleAxisd(-M_PI/2, Eigen::Vector3d::UnitZ());
            res = robot_.moveTipIK(Eigen::Vector3d(0.0, -1.0, 1.0), q) ;
        }

        gui_.sendMessageToCameraView("Click on the image to select gripper tip point") ;

        return res ;
    }

    void onMouseClicked(uint x, uint y) {

        image_geometry::PinholeCameraModel cm ;
        cv::Mat clr, depth ;

        ros::Time ts ;

        float z ;
        cv::Point3d p ;

        int nPts = (fixed_) ? 9 : max_points_;

        cap_.grab(clr, depth, ts, cm) ;

        cout << (depth.type() == CV_32FC1) << endl  ;

        if ( cap_.grab(clr, depth, ts, cm) &&
                sampleBilinearDepth(depth, x, y, z) ) {

            p = cm.projectPixelTo3dRay(cv::Point2d(x, y));
            p *= z/1000.0;

           // cout << x << ' ' << y << ' ' << z << ' ' << p.x << ' ' << p.y << ' ' << ' ' << p.z << endl;

            Vector3d rp = robot_.getTipPose().translation();

            // add the point to the list

            pts_.push_back(p);
            rpts_.push_back(rp);

            current_pt_++;
        }
        else return ;


        if (current_pt_ == nPts) {
            // all points have been added, do the calibration

            doCalibrate(pts_, rpts_, out_folder_, camera_);

            calibration_finished = true;

            gui_.sendMessageToCameraView("Calibration finished");
            return;
        }
        else
        {
            int c = current_pt_ ;

            gui_.sendMessageToCameraView("Moving the robot to the next position") ;
            if ( fixed_ ) {
                robot_.moveTipIK(Eigen::Vector3d(points_[c][0], points_[c][1], points_[c][2]),
                                 lookAt(Vector3d(-1, 0, 0), M_PI));
            }
            else
            {
                while (1)
                {
                    double X = minX + (maxX - minX)*(rand()/(double)RAND_MAX) ;
                    double Y = minY + (maxY - minY)*(rand()/(double)RAND_MAX) ;
                    double Z = minZ + (maxZ - minZ)*(rand()/(double)RAND_MAX) ;

                    const double qscale = 0.3 ;
                    double qx = qscale * (rand()/(double)RAND_MAX - 0.5) ;
                    double qy = qscale * (rand()/(double)RAND_MAX - 0.5) ;
                    double qz = qscale * (rand()/(double)RAND_MAX - 0.5) ;
                    double qw = qscale * (rand()/(double)RAND_MAX - 0.5) ;

                    Quaterniond q = robot_helpers::lookAt(Vector3d(-1, 0, 0), M_PI) ;

                    if ( robot_.moveTipIK( Eigen::Vector3d(X, Y, Z), q ) ) break ;
                }
            }

            stringstream msg ;
            msg << "click on image to select the gripper tip point: " << current_pt_ << " of " << nPts ;
            gui_.sendMessageToCameraView(msg.str()) ;
        }


    }


private:

    const size_t max_points_ = 30 ;

    int current_pt_ = 0;

    vector<cv::Point3d> pts_ ;
    vector<Vector3d> rpts_ ;
    robot_helpers::RobotArm &robot_ ;
    viz_helpers::CameraViewClient &gui_ ;
    camera_helpers::OpenNICaptureRGBD cap_ ;
    string out_folder_, camera_ ;
    bool fixed_ = false ;
 };


int main(int argc, char **argv) {

    ros::init(argc, argv, "calibrate_fixed_xtion");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    int c = 1 ;

    string outFolder ;

    while ( c < argc )
    {

        if ( strncmp(argv[c], "--out", 5) == 0 )
        {
            if ( c + 1 < argc ) {
                outFolder = argv[++c] ;
            }
        }
        else if ( strncmp(argv[c], "--stations", 10) == 0 )
        {
            if ( c + 1 < argc ) {
                maxPoints = atoi(argv[++c]) ;
            }
        }
        else if ( strncmp(argv[c], "--bbox", 6) == 0 )
        {
            if ( c + 1 < argc ) {
                minX = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                minY = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                minZ = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                maxX = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                maxY = atof(argv[++c]) ;
            }
            if ( c + 1 < argc ) {
                maxZ = atof(argv[++c]) ;
            }

        }

        ++c ;
    }

    nh.getParam("/calibration_folder", outFolder) ;

    srand(clock()) ;

    //ros::Publisher vis_pub = nh.advertise<visualization_msgs::Marker>( "visualization_marker", 0 );

    viz_helpers::CameraViewClient gui ;
    RobotArm arm("r2") ;

    arm.moveHome() ;

    Calibrator calib("xtion3", outFolder, arm, gui) ;

    // open grabber and wait until connection

    if ( ! calib.init() )
    {
        ROS_ERROR("can't connect to openni camera") ;
        exit(1) ;
    }


//    rb.setCurrentGroup("r1");
 //   rb.moveGripperToPose( Vector3d(-1, -0.5, 1), robot_helpers::lookAt(Vector3d(0, 0, -1))) ;

//    Context ctx ;
//    ctx.currentPoint = 0 ;
//    ctx.robot = &rb ;
 //   ctx.gui = &srv ;
//    ctx.fixed = false ;



    // wait until calibration has finished

    while (!calibration_finished) ;

    //rb.setServoPowerOff() ;

    ros::shutdown();
    return 1;
}
