#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <kdl/frames.hpp>
#include <tf_conversions/tf_kdl.h>
#include <string.h>
#include <fstream>

using namespace std ;

bool readCalibData(const string &filename, tf::Transform &transform)
{
    std::ifstream in(filename.c_str());

    if (!in) {
        ROS_WARN_STREAM("Can not find file " << filename << ", using default calibration.");
        return false;
    }

    std::vector<float> data;
    std::copy(std::istream_iterator<float>(in), std::istream_iterator<float>(), std::back_inserter(data)) ;

    if (data.size() < 12) {
        ROS_WARN_STREAM("Invalid calibration data format, using default calibration");
        return false;
    }

    transform.setBasis(tf::Matrix3x3(data[0], data[1], data[2], data[4], data[5], data[6], data[8], data[9], data[10])) ;
    transform.setOrigin(tf::Vector3(data[3], data[7], data[11])) ;
}

int main(int argc, char** argv) {
    ros::init(argc, argv, "handeye_calibration_publisher");
	ros::NodeHandle node("~");

    std::string calib_path, parent_frame, camera_frame ;
    node.param<std::string>("data_path", calib_path, "");
    node.param<std::string>("parent_frame", parent_frame, "");
    node.param<std::string>("camera_frame", camera_frame, "");

    if ( calib_path.empty() ) {
        ROS_ERROR("No calibration data file provided") ;
        return 0 ;
    }

    if ( parent_frame.empty() )  {
        ROS_ERROR("The parent_frame parameter was not set") ;
        return 0 ;
    }

    if ( camera_frame.empty() )  {
        ROS_ERROR("The camera_frame parameter was not set") ;
        return 0 ;
    }

    tf::Transform transform ;

    if ( readCalibData(calib_path, transform) )  {

        static tf::TransformBroadcaster br;

        while (ros::ok()) {
            br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), parent_frame, camera_frame));
            ros::Duration(0.01).sleep();
        }
    }

	return 0;
}


