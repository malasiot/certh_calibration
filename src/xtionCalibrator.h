#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <vector>
#include <string>

class xtionCalibrator {

private:

        //move xtion around fixed point
    std::vector<Eigen::Vector3d> xtionDepthPoints;  //i,j of depth image & depth value
    std::vector<Eigen::Matrix4d, Eigen::aligned_allocator<Eigen::Matrix4d> >
pointsRotMats;                //
        double mFx,mFy,mCx,mCy;

public:
        xtionCalibrator();
    void setData(std::vector<Eigen::Vector3d> xtionPoints_,
std::vector<Eigen::Matrix4d, Eigen::aligned_allocator<Eigen::Matrix4d> >
pointsRotMat_);
        Eigen::Vector3d calcYVec(std::vector<Eigen::Vector3d> xtionPoints,
std::vector<Eigen::Vector3d> armPoints);
        bool calcFx(std::vector<Eigen::Vector3d> xtionPoints, std::vector<Eigen::Vector3d>
armPoints);
        bool calcFy(std::vector<Eigen::Vector3d> xtionPoints, std::vector<Eigen::Vector3d>
armPoints);
        bool calcCxy(std::vector<Eigen::Vector3d> xtionPoints, double step);
        bool calibrateAll();
        bool calibrateIntrinsics();
        bool calibrateIntrinsics2(std::vector<Eigen::Vector3d> xtionPoints,
std::vector<Eigen::Vector3d> armPoints); //arm Points parallel to x axis of xtion
        bool calibrateExtrinsics();
        bool writeIntrinsics(std::string fname);

};
