#include "calibration.h"

#include <highgui.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui.hpp>

#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

#include <Eigen/Eigenvalues>


using namespace std ;
using namespace Eigen ;

//using namespace cvx::util ;

int counter = 0 ;
static bool findCorners(const cv::Mat &im, const cv::Size &boardSize, vector<cv::Point2f> &corners)
{
    cv::Mat gray ;

    cv::cvtColor(im, gray, CV_BGR2GRAY) ;

    //CALIB_CB_FAST_CHECK saves a lot of time on images
    //that do not contain any chessboard corners

    bool patternfound = cv::findChessboardCorners(gray, boardSize, corners,
            cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE );

    if ( patternfound && (corners[0].y > corners[boardSize.width].y) )
    {
        // reverse direction

        vector<cv::Point2f> corners_ ;

        for( int i=corners.size()-1 ; i>=0 ; i-- )
            corners_.push_back(corners[i]) ;

        corners = corners_ ;

    }

    if(patternfound)
        cv::cornerSubPix(gray, corners, cv::Size(5, 5), cv::Size(-1, -1),
            cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));


    cv::drawChessboardCorners(im, boardSize, cv::Mat(corners), patternfound);

    stringstream str ;

    str << "calib_pts_" << counter++  << ".png" ;

    cv::imwrite("/tmp/" + str.str(), im) ;



    return patternfound ;
}

static double computeReprojectionErrors(
        const vector<vector<cv::Point3f> >& objectPoints,
        const vector<vector<cv::Point2f> >& imagePoints,
        const vector<cv::Mat>& rvecs, const vector<cv::Mat>& tvecs,
        const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs,
        vector<float>& perViewErrors )
{
    vector<cv::Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for( i = 0; i < (int)objectPoints.size(); i++ )
    {

        projectPoints(cv::Mat(objectPoints[i]), rvecs[i], tvecs[i],
                      cameraMatrix, distCoeffs, imagePoints2);
        err = norm(cv::Mat(imagePoints[i]), cv::Mat(imagePoints2), CV_L2);
        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float)std::sqrt(err*err/n);
        totalErr += err*err;
        totalPoints += n;
    }

    return std::sqrt(totalErr/totalPoints);
}



static void findExtrinsics( const vector<vector<cv::Point2f> > &imagePoints,
                            const vector<vector<cv::Point3f> > &objectPoints,
                            const cv::Size &imageSize,
                            const cv::Mat& cameraMatrix,
                            vector<cv::Mat>& rvecs, vector<cv::Mat>& tvecs,
                            double& totalAvgErr)
{

    cv::Mat distCoeffs = cv::Mat::zeros(8, 1, CV_64F);

    for(int i=0 ; i<imagePoints.size() ; i++ )
    {

        cv::Mat rvec, tvec ;
        cv::solvePnP(objectPoints[i], imagePoints[i], cameraMatrix, distCoeffs, rvec, tvec) ;
        rvecs.push_back(rvec) ;
        tvecs.push_back(tvec) ;
    }

    vector<float> reprojErrs ;

    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);


    return ;
}

Eigen::Affine3d randomAffine(double scale=0.1)
{

    Vector3d trans = 0.1 * Vector3d::Random() ;
    Vector4d rot = Vector4d::Random()*scale ;
    rot.normalize() ;

    return Translation3d(trans) * Quaterniond(rot).toRotationMatrix() ;
}

void estimateTargetToSensorProjective(const vector<vector<cv::Point2f> > &img_corners, const vector<vector<cv::Point3f> > &obj_corners,
                                      const cv::Mat &camMatrix, vector<Eigen::Affine3d> & target_to_sensor)
{
    vector<cv::Mat> rvecs, tvecs;
    vector<float> reprojErrs;

    double totalAvgErr;

    findExtrinsics(img_corners, obj_corners, cv::Size(640, 480),  camMatrix,  rvecs, tvecs, totalAvgErr) ;

    cout << "reprojection error: " << totalAvgErr << endl ;

    for(unsigned int i=0 ; i<tvecs.size() ; i++ )
    {
        Matrix3d r ;
        Vector3d t ;

        cv::Mat rmat_ ;
        cv::Rodrigues(rvecs[i], rmat_) ;

        cv::Mat_<double> rmat(rmat_) ;
        cv::Mat_<double> tmat(tvecs[i]) ;

        r << rmat(0, 0), rmat(0, 1), rmat(0, 2), rmat(1, 0), rmat(1, 1), rmat(1, 2), rmat(2, 0), rmat(2, 1), rmat(2, 2) ;
        t << tmat(0, 0), tmat(0, 1), tmat(0, 2) ;

        Affine3d tr = Translation3d(t) * r ;

        target_to_sensor.push_back(tr) ;
    }

}

static double estimatePose(const vector<Vector3d> &pts1, const vector<Vector3d> &pts2, double *w,
      Affine3d &Rt)
{

    assert(pts1.size() == pts2.size()) ;

    int i ;

    Vector3d mp1(0, 0, 0), mp2(0, 0, 0) ;

    double wsum = 0.0 ;

    for( i=0 ; i<pts1.size() ; i++ )
    {
        const Vector3d &p1 = pts1[i] ;
        const Vector3d &p2 = pts2[i] ;

        wsum += w[i] ;

        mp1 += w[i] * p1 ; mp2 += w[i] * p2 ;
    }

    mp1 /= wsum ; mp2 /= wsum ;


  // Use Horn's quaternion solution

    Matrix4d K ;

    for( i=0 ; i<pts1.size() ; i++ )
    {
        Vector3d y1 = pts1[i] - mp1 ;
        Vector3d y2 = pts2[i] - mp2 ;
        Vector3d y1m2 = y1 - y2 ;
        Vector3d y1p2 = y2 + y1 ;

        Matrix4d A2 ;
        A2 <<  0.0, -y2.x(), -y2.y(), -y2.z(),
                 y2.x(),    0.0, -y2.z(),  y2.y(),
                 y2.y(),  y2.z(),    0.0, -y2.x(),
                 y2.z(), -y2.y(),  y2.x(),    0.0 ;

        Matrix4d A1 ;
        A1 <<  0.0, -y1.x(), -y1.y(), -y1.z(),
                 y1.x(),    0.0,  y1.z(), -y1.y(),
                 y1.y(), -y1.z(),    0.0,  y1.x(),
                y1.z(),  y1.y(), -y1.x(),  0.0 ;

        K += w[i] * A1.transpose() * A2 ;
    }

    Matrix4d U ;
    Vector4d L ;

    SelfAdjointEigenSolver<Matrix4d> es;
    es.compute(K) ;

    L = es.eigenvalues() ;
    U = es.eigenvectors() ;

    Quaterniond rq(U(0, 0), U(1, 0), U(2, 0), U(3, 0)) ;

    Matrix3d RR = rq.toRotationMatrix() ;

    Vector3d T = mp2 - RR * mp1 ;

    double err = 0.0 ;

    for( i=0 ; i< pts1.size() ; i++ )
    {
        Vector3d p1 = pts1[i];
        Vector3d p2 = pts2[i]  ;

        Vector3d p1t = RR*p1 + T ;
        Vector3d pd = p2 - RR*p1 - T ;


        err += pd.norm() ;
    }

    Rt = Translation3d(T)*RR ;

    return err/pts1.size() ;

}

#if 0
void estimateTargetToSensorRigid(const vector<vector<cv::Point3f> > &img_corners, const vector<vector<cv::Point3f> > &obj_corners,
                                      const cv::Mat &camMatrix, vector<Affine3d> & target_to_sensor)
{
    assert(img_corners.size() == obj_corners.size()) ;

    int nFrames = img_corners.size() ;

    double fx = camMatrix.at<double>(0, 0) ;
    double fy = camMatrix.at<double>(1, 1) ;
    double cx = camMatrix.at<double>(0, 2) ;
    double cy = camMatrix.at<double>(1, 2) ;

    for(int i=0 ; i<nFrames ; i++ )
    {
        const vector<cv::Point3f> &ic = img_corners[i] ;
        const vector<cv::Point3f> &oc = obj_corners[i] ;

        vector<Eigen::Vector3f> srcPts, dstPts ;
        vector<Eigen::Vector3d> dstPts_ ;

        const double scale = 1.0e-3 ;

        for(int j=0 ; j<ic.size() ; j++ )
        {
            float z = ic[j].z ;

            if ( z == 0 ) continue ;

            Eigen::Vector3f ip( scale * ( ic[j].x - cx ) * z / fx, scale * ( ic[j].y - cy ) * z / fy, scale * z) ;
            cv::Point3f op = oc[j] ;

            srcPts.push_back(ip) ;
            dstPts.push_back(Eigen::Vector3f(op.x, op.y, op.z)) ;
            dstPts_.push_back(Eigen::Vector3d(ip.x(), ip.y(), ip.z())) ;
        }

        Vector3d c, u ;
        double err = certh_libs::robustPlane3DFit(dstPts_, c, u) ;

    //    cout << err << endl ;

        int nPts = srcPts.size() ;

        MatrixXf src(3, nPts), dst(3, nPts) ;

        for( int j=0 ; j<nPts ; j++ )
        {
            src.col(j) = srcPts[j] ;
            dst.col(j) = dstPts[j] ;
        }

        MatrixXf res = umeyama(dst, src, false) ;

        Affine3d tr ;
        tr.matrix() <<  res(0, 0), res(0, 1), res(0, 2), res(0, 3),
                        res(1, 0), res(1, 1), res(1, 2), res(1, 3),
                        res(2, 0), res(2, 1), res(2, 2), res(2, 3),
                        res(3, 0), res(3, 1), res(3, 2), res(3, 3);


        target_to_sensor.push_back(tr) ;


    }
}

void estimateTargetToSensorRigid(const vector<vector<cv::Point3f> > &img_corners, const vector<vector<cv::Point3f> > &obj_corners,
                                      const cv::Mat &camMatrix, vector<Affine3d> & target_to_sensor)
{
    assert(img_corners.size() == obj_corners.size()) ;

    int nFrames = img_corners.size() ;

    double fx = camMatrix.at<double>(0, 0) ;
    double fy = camMatrix.at<double>(1, 1) ;
    double cx = camMatrix.at<double>(0, 2) ;
    double cy = camMatrix.at<double>(1, 2) ;


    for(int i=0 ; i<nFrames ; i++ )
    {
        const vector<cv::Point3f> &ic = img_corners[i] ;
        const vector<cv::Point3f> &oc = obj_corners[i] ;

        vector<Eigen::Vector3d> srcPts, dstPts ;
        vector<Eigen::Vector3d> dstPts_ ;

        const double scale = 1.0e-3 ;

        for(int j=0 ; j<ic.size() ; j++ )
        {
            float z = ic[j].z ;

            if ( z == 0 ) continue ;

            Eigen::Vector3d ip( scale * ( ic[j].x - cx ) * z / fx, scale * ( ic[j].y - cy ) * z / fy, scale * z) ;
            cv::Point3d op = oc[j] ;

            srcPts.push_back(ip) ;
            dstPts.push_back(Eigen::Vector3d(op.x, op.y, op.z)) ;
            dstPts_.push_back(Eigen::Vector3d(ip.x(), ip.y(), ip.z())) ;
        }

        Vector3d c, u ;
        double err = certh_libs::robustPlane3DFit(dstPts_, c, u) ;

        int nPts = srcPts.size() ;

        MatrixXf src(3, nPts), dst(3, nPts) ;

        double *w = new double [nPts] ;

        for( int j=0 ; j<nPts ; j++ )
            w[j] = 1.0 ;

        Affine3d tr ;
        double err1 = estimatePose(dstPts, srcPts, w, tr) ;

        target_to_sensor.push_back(tr) ;


    }
}
#endif

void find_target_motions(const string &filePrefix, const string &dataFolder, const cv::Size boardSize, const double squareSize, cv::Mat &cameraMatrix, bool useDepth,
                           vector<Affine3d> &gripper_to_base, vector<Affine3d> &target_to_sensor )
{
    string fileSuffix = "_c.*" ;

    namespace fs = boost::filesystem  ;

    std::string fileNameRegex = filePrefix + "([[:digit:]]+)" + fileSuffix ;

    boost::regex rx(fileNameRegex) ;

    vector<vector<cv::Point2f> > img_corners ;
    vector<vector<cv::Point3f> > obj_corners, img_corners3 ;

    fs::directory_iterator it(dataFolder), end ;

    for( ; it != end ; ++it)
    {
        fs::path p = (*it).path() ;

        string fileName = p.filename().string() ;

        boost::smatch sm ;

        // test of this is a valid filename

        if ( !boost::regex_match(fileName, sm, rx ) ) continue ;

        // read color image

        cv::Mat im = cv::imread(p.string(), -1) ;

        // read robot pose

        Affine3d tr ;

        {
            boost::replace_all(fileName,"_c.png", "_pose.txt") ;

            string filePath = p.parent_path().string() + '/' + fileName ;
            ifstream strm(filePath.c_str()) ;

            Matrix3d r ;
            Vector3d t ;

            double vals[12] ;

            for(int i=0 ; i<12 ; i++ ) strm >> vals[i] ;

            r << vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7], vals[8] ;
            t << vals[9], vals[10], vals[11] ;

            tr = Translation3d(t) * r ;
        }

        // read depth image

        cv::Mat dim ;

        if ( useDepth )
        {
            boost::replace_all(fileName, "_pose.txt", "_d.png") ;

            string filePath = dataFolder + '/' + fileName ;

            dim = cv::imread(filePath, -1) ;

        }

        img_corners.push_back(vector<cv::Point2f>()) ;
        img_corners3.push_back(vector<cv::Point3f>()) ;
        obj_corners.push_back(vector<cv::Point3f>()) ;

        if ( findCorners(im, boardSize, img_corners.back()) )
        {
            vector<cv::Point2f> &ic = img_corners.back() ;

            for( int i = 0, k=0; i < boardSize.height; i++ )
                for( int j = 0; j < boardSize.width; j++, k++ )
                {
                    if ( useDepth )
                    {
                        float z ;

                        if ( sampleBilinearDepth(dim, ic[k].x, ic[k].y, z) )
                            img_corners3.back().push_back(cv::Point3f(ic[k].x, ic[k].y, z)) ;
                        else
                            img_corners3.back().push_back(cv::Point3f(ic[k].x, ic[k].y, 0.0)) ;

                    }
                    obj_corners.back().push_back(cv::Point3f(float(j*squareSize), float(i*squareSize), 0.0));
                }

            gripper_to_base.push_back(tr) ;
        }
        else
        {
            img_corners.pop_back() ;
            obj_corners.pop_back() ;
            img_corners3.pop_back() ;
        }

    }

    if ( useDepth )
        estimateTargetToSensorRigid(img_corners3, obj_corners, cameraMatrix, target_to_sensor) ;
    else
        estimateTargetToSensorProjective(img_corners, obj_corners, cameraMatrix, target_to_sensor) ;

}


void benchmark(const string &filePrefix, const string &dataFolder, const cv::Size boardSize, const double squareSize, cv::Mat &camMatrix, bool useDepth,
               const Affine3d &target_to_gripper, const Affine3d &sensor_to_base)
{
    double fx = camMatrix.at<double>(0, 0) ;
    double fy = camMatrix.at<double>(1, 1) ;
    double cx = camMatrix.at<double>(0, 2) ;
    double cy = camMatrix.at<double>(1, 2) ;

    vector<Affine3d> gripper_to_base, target_to_sensor ;

    string fileSuffix = "_c.*" ;

    namespace fs = boost::filesystem  ;

    std::string fileNameRegex = filePrefix + "([[:digit:]]+)" + fileSuffix ;

    boost::regex rx(fileNameRegex) ;

    vector<vector<cv::Point2f> > img_corners ;
    vector<vector<cv::Point3f> > obj_corners, img_corners3 ;

    fs::directory_iterator it(dataFolder), end ;

    for( ; it != end ; ++it)
    {
        fs::path p = (*it).path() ;

        string fileName = p.filename().string() ;

        boost::smatch sm ;

        // test of this is a valid filename

        if ( !boost::regex_match(fileName, sm, rx ) ) continue ;

        // read color image

        cv::Mat im = imread(p.string(), -1) ;

        // read robot pose

        Affine3d tr ;

        {
            boost::replace_all(fileName,"_c.png", "_pose.txt") ;

            string filePath = p.parent_path().string() + '/' + fileName ;
            ifstream strm(filePath.c_str()) ;

            Matrix3d r ;
            Vector3d t ;

            double vals[12] ;

            for(int i=0 ; i<12 ; i++ ) strm >> vals[i] ;

            r << vals[0], vals[1], vals[2], vals[3], vals[4], vals[5], vals[6], vals[7], vals[8] ;
            t << vals[9], vals[10], vals[11] ;

            tr = Translation3d(t) * r ;
        }

        Vector3d tip = tr.translation() ;

        Vector3d tip_to_sensor = sensor_to_base.inverse() * tip ;

        float x = fx * tip_to_sensor.x() / tip_to_sensor.z() + cx ;
        float y = fy * tip_to_sensor.y() / tip_to_sensor.z() + cy ;

        cv::circle(im, cv::Point(x, y), 3, cv::Scalar(0, 255, 0), 2) ;

        cv::imwrite("/tmp/benchmark.png", im) ;

    }



}
