#include <ros/ros.h>
#include <camera_helpers/OpenNIServiceClient.h>
#include <viz_helpers/CameraViewServer.h>
#include <robot_helpers/Robot2.h>
#include <robot_helpers/Utils2.h>
#include <robot_helpers/Geometry.h>
#include <visualization_msgs/Marker.h>
#include <certh_libs/Helpers.h>
#include <fstream>
#include <highgui.h>
#include <pcl/io/pcd_io.h>
#include "xtionCalibrator.h"

using namespace std ;
using namespace Eigen ;
using namespace robot_helpers ;

string camera = "xtion1", armName, arm2Name ;
bool calibration_finished ;
const Eigen::Vector3d arm2Point (1,      -1.3 ,    1), arm1Point (-1,      -1.3 ,    1) ;
const Eigen::Vector3d arm1StartingPoint (-0.5, -1 ,1), arm2StartingPoint (0.5, -1 ,1) ;
int phase ;
int nPoints ;
int currPoint ;
float step ;
std::vector < Eigen::Vector3d > relativePoints;
xtionCalibrator calib;
Eigen::Vector3d xtionYVec, xtionZVec;
float meanError ;
int counter ;

Eigen::Vector3d groundTruth(-1.2,0,0.72) ;


void init(std::vector < Eigen::Vector3d > &positions, std::vector < Eigen::Quaterniond > &orientations){


    counter = 0 ;

    positions.push_back(Eigen::Vector3d (-0.979, 0.184, 1.413));
    positions.push_back(Eigen::Vector3d (-0.979, -0.204, 1.413));
    positions.push_back(Eigen::Vector3d (-0.978, -0.031, 1.380));
    positions.push_back(Eigen::Vector3d (-0.980, -0.031, 1.311));
    positions.push_back(Eigen::Vector3d (-0.988, 0.137, 1.248));
    positions.push_back(Eigen::Vector3d (-0.989, -0.181, 1.245));
    positions.push_back(Eigen::Vector3d (-1.141, -0.010, 1.176));
    positions.push_back(Eigen::Vector3d (-1.132, 0.004, 1.213));
    positions.push_back(Eigen::Vector3d (-1.133, 0.287, 1.203));
    positions.push_back(Eigen::Vector3d (-1.133, 0.269, 1.188));



    orientations.push_back(Eigen::Quaterniond (-0.275, 0.769, -0.560, 0.140));
    orientations.push_back(Eigen::Quaterniond (-0.275, 0.769, -0.560, 0.140));
    orientations.push_back(Eigen::Quaterniond (-0.161, 0.781, -0.603, -0.012));
    orientations.push_back(Eigen::Quaterniond (-0.287, 0.766, -0.554, 0.157));
    orientations.push_back(Eigen::Quaterniond (-0.061, 0.769, -0.621, -0.139));
    orientations.push_back(Eigen::Quaterniond (-0.050, 0.767, -0.622, -0.152));
    orientations.push_back(Eigen::Quaterniond (-0.021, 0.781, -0.623, -0.034));
    orientations.push_back(Eigen::Quaterniond (-0.157, 0.770, -0.603, 0.137));
    orientations.push_back(Eigen::Quaterniond (-0.121, 0.777, -0.611, 0.092));
    orientations.push_back(Eigen::Quaterniond (0.229, 0.752, -0.496, 0.369));

}
struct Context {

    int currentPoint ;
    vector<pcl::PointXYZ> pts ;
    vector<Vector3d> rpts ;
    robot_helpers::Robot2 *robot ;
    viz_helpers::CameraViewServer *gui ;
    string outFolder, camera ;
    bool fixed ;

    std::vector <Eigen::Vector3d> xtionDepthPoints ;
    std::vector <Eigen::Vector3d> armPoints ;
    int maxPoints  ;
};

ofstream myfile;

void getXY(int& x, int& y, cv::Mat depth){

    bool found = false;
    int col,row;
    for(col =x; col<depth.cols; ++col){
        for(row = y; row<depth.rows; row++){
            unsigned short d = depth.at<unsigned short>(row,col);
            if(d != 0 && d < 7000){
                found = true;
                break;
            }
        }
        if(found)
            break;
    }
    x = col;
    y = row;


}

void onMouseClicked(int x, int y, Context *ctx, std::vector < Eigen::Vector3d > positions, std::vector < Eigen::Quaterniond > orientations)
{

    counter++ ;

    if ( counter >= orientations.size()){
        cout << "poses finished!" << endl ;
        calibration_finished = true ;
        return ;
    }

    image_geometry::PinholeCameraModel cm ;
    cv::Mat clr, depth ;
    ros::Time ts ;
    pcl::PointCloud <pcl::PointXYZ> pc ;
    camera_helpers::openni::grab(camera, clr, depth, pc, ts, cm) ;

    getXY(x,y,depth);

    std::cout << " -------------------------------------------------------------------- " << std::endl ;
    std::cout<< "point (" << x << ", " << y << ") distance :  " << depth.at<unsigned short>(y,x) <<  std::endl;

    pcl::PointXYZ pcPoint = pc.at(x,y) ;
    std::cout << "point from camera : " << pcPoint.x << " " <<  pcPoint.y << " " <<  pcPoint.z << std::endl;

    Eigen::Vector4d  targetP ;
    Eigen::Matrix4d calib = ctx->robot->getTransform("xtion1_rgb_optical_frame").matrix() ;
    Eigen::Vector4d tar(pcPoint.x, pcPoint.y, pcPoint.z, 1);
    targetP = calib.inverse() * tar ;

    std::cout << "point from base_link : " << targetP.x() << " " <<  targetP.y() << " " <<  targetP.z() << std::endl;
    cout << "point error : " << abs(groundTruth.x()) - abs(targetP.x()) << ",  " << abs(groundTruth.y()) - abs(targetP.y()) << ", " << abs(groundTruth.z()) - abs(targetP.z()) << endl;

    float error ;
    error = sqrt(pow((abs(groundTruth.x())  - abs(targetP.x())),2 ) + pow((abs(groundTruth.y())  - abs(targetP.y())),2 ) + pow((abs(groundTruth.z())  - abs(targetP.z())),2 ) ) ;

    meanError += error ;

    cout << "square error : " << error << endl;

    moveArm(*(ctx->robot),positions[counter],orientations[counter],"_ee") ;

}

void createRelativePointMatrix(float dx, float dz){

    relativePoints.push_back( Eigen::Vector3d (0, -dx , -dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, dx , dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, -dx , dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, dx , -dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, 0 , -dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0,  0 , dz)) ;
    relativePoints.push_back( Eigen::Vector3d (0, -dx , 0)) ;
    relativePoints.push_back( Eigen::Vector3d (0, dx , 0)) ;

}


int main(int argc, char **argv) {

    ros::init(argc, argv, "calibrate_arm_xtion");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(4) ;
    spinner.start() ;

    camera_helpers::openni::connect("xtion1") ;

    viz_helpers::CameraViewServer srv ;
    Context ctx ;
    Robot2 rb("r1");

    std::vector < Eigen::Vector3d > positions ;
    std::vector < Eigen::Quaterniond > orientations ;
    init(positions, orientations) ;
    meanError = 0 ;

    moveArm(rb, positions[0], orientations[0], "_ee") ;

    ctx.currentPoint = 0 ;
    ctx.robot = &rb ;
    ctx.gui = &srv ;
    ctx.fixed = true ;

    srv.mouseClicked.connect(boost::bind(onMouseClicked, _1, _2, &ctx, positions, orientations));

    srv.sendMessage("Click on the image to select gripper tip point") ;



    while (!calibration_finished) ;

    meanError/= (float) positions.size() ;

    cout << "TOTAL ERROR : " << meanError << endl ;

    rb.moveHomeArms() ;
    rb.setServoPowerOff();
}
