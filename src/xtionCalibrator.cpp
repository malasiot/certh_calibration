#include "xtionCalibrator.h"
#include <iostream>

xtionCalibrator::xtionCalibrator() {

        mFx = 0;
        mFy = 0;
        mCx = 0;
        mCy = 0;

}

void xtionCalibrator::setData(std::vector<Eigen::Vector3d> xtionDepthPoints_,
std::vector<Eigen::Matrix4d, Eigen::aligned_allocator<Eigen::Matrix4d> >
pointsRotMats_) {

    xtionDepthPoints = xtionDepthPoints_;
    pointsRotMats= pointsRotMats_;

}


Eigen::Vector3d xtionCalibrator::calcYVec(std::vector<Eigen::Vector3d> xtionPoints,
std::vector<Eigen::Vector3d> armPoints){

        double sinTheta = 0;
        for(int i=1; i<armPoints.size(); ++i)
                sinTheta += (xtionPoints[i](2) - xtionPoints[i-1](2)) / sqrt( pow(armPoints[i](0)
- armPoints[i-1](0), 2) + pow(armPoints[i](1) - armPoints[i-1](1), 2) +
pow(armPoints[i](2) - armPoints[i-1](2), 2) );

        sinTheta /= xtionPoints.size() - 1;

        double sign = xtionPoints[1](2) > xtionPoints[0](2) ? 1:-1;
        return Eigen::Vector3d(sinTheta * sign, 0, sqrt(1 - pow(sinTheta, 2))*sign );

}


bool xtionCalibrator::calcFx(std::vector<Eigen::Vector3d> xtionPoints,
std::vector<Eigen::Vector3d> armPoints){
        double fx = 0;
        for(int i=1; i<xtionPoints.size(); ++i)
                fx += abs(xtionPoints[i](0) - xtionPoints[i-1](0)) * ( (xtionPoints[i](2) +
xtionPoints[i-1](2))/2 ) / sqrt( pow(armPoints[i](0) - armPoints[i-1](0), 2) +
pow(armPoints[i](1) - armPoints[i-1](1), 2) + pow(armPoints[i](2) -
armPoints[i-1](2), 2) );

        fx /= xtionPoints.size() - 1;

        mFx = fx;
        return true;

}


bool xtionCalibrator::calcFy(std::vector<Eigen::Vector3d> xtionPoints,
std::vector<Eigen::Vector3d> armPoints){
        double fy = 0;
        for(int i=1; i<xtionPoints.size(); ++i)
                fy += abs(xtionPoints[i](1) - xtionPoints[i-1](1)) * ( (xtionPoints[i](2) +
xtionPoints[i-1](2))/2 ) / sqrt( pow(armPoints[i](0) - armPoints[i-1](0), 2) +
pow(armPoints[i](1) - armPoints[i-1](1), 2) + pow(armPoints[i](2) -
armPoints[i-1](2), 2) );

        fy /= xtionPoints.size() - 1;

        mFy = fy;
        return true;
}

bool xtionCalibrator::calcCxy(std::vector<Eigen::Vector3d> xtionPoints, double step){

        double cx = 0;
        double cy = 0;
        for(int i=1; i<xtionPoints.size(); ++i){
                cx += (step*mFx - xtionPoints[i](0)*xtionPoints[i](2) + xtionPoints[i-1](0)*xtionPoints[i-1](2)) / (xtionPoints[i-1](2) - xtionPoints[i](2));
                cy += (step*mFy - xtionPoints[i](1)*xtionPoints[i](2) + xtionPoints[i-1](1)*xtionPoints[i-1](2)) / (xtionPoints[i-1](2) - xtionPoints[i](2));
        }

        cx /= xtionPoints.size() - 1;
        cy /= xtionPoints.size() - 1;

        mCx = cx;
        mCy = cy;

        return true;

}


//arm Points parallel to x axis of xtion
bool calibrateIntrinsics2(std::vector<Eigen::Vector3d> xtionPoints,
std::vector<Eigen::Vector3d> armPoints){

        int c1 = 0;
        int c2 = xtionPoints.size() / 3;
        int c3 = xtionPoints.size() * 2 / 3;
        int c4 = xtionPoints.size();



        double fy = 0;
        for(int i=c2+1; i<c3; ++i)
                fy += (xtionPoints[i](1) - xtionPoints[c2](1)) * ( (xtionPoints[c2](2) +
xtionPoints[i](2))/2 ) / sqrt( pow(armPoints[c2](0) - armPoints[i](0), 2) +
pow(armPoints[c2](1) - armPoints[i](1), 2) + pow(armPoints[c2](2) -
armPoints[i](2), 2) );

        fy /= (c3 - c2) - 1;

        double cx = 0;
        double cy = 0;
        for(int i=c3+1; i<c4; ++i){

        }
        return true;

}


bool xtionCalibrator::calibrateIntrinsics(){

        if(xtionDepthPoints.size() != pointsRotMats.size())
                return false;
        /*
    std::vector<Eigen::Matrix4d> invMats;
    for(int i=1; i<pointsRotMats.size(); ++i){
        invMats.push_back(pointsRotMats[0].inverse() * pointsRotMats[i]);
    }


    double minDist = 1000000000;
    for(int fx=580; fx < 630; ++fx){
        for(int fy=580; fy<630; ++fy){
            for(int cx = 250; cx < 400; ++cx){
                for(int cy = 180; cy < 320; ++cy){

                    double z0 = xtionDepthPoints[0](2) / 1000.0;
                    double x0 = (xtionDepthPoints[0](0) - double(cx)) * z0 /
double(fx);
                    double y0 = (xtionDepthPoints[0](1) - double(cy)) * z0 /
double(fy);
                    double dist = 0;
                    for(int i=1; i<xtionDepthPoints.size(); ++i){

                        double zi = xtionDepthPoints[i](2) / 1000.0;
                        double xi = (xtionDepthPoints[i](0) - double(cx)) * zi /
double(fx);
                        double yi = (xtionDepthPoints[i](1) - double(cy)) * zi /
double(fy);
                        Eigen::Vector4d vi(xi, yi, zi, 1);
                        vi  = invMats[i-1] * vi;

                        dist = dist + pow(x0-vi(0), 2) + pow(y0-vi(1), 2) +
pow(z0-vi(2), 2);
                    }
                    if(dist < minDist){
                        minDist = dist;
                        mFx = fx;
                        mFy = fy;
                        mCx = cx;
                        mCy = cy;
                    }
                }
            }
        }
        std::cout << fx << std::endl;
    }
    std::cout << "minDist: " << sqrt(minDist) << std::endl;

        */


    int nEq = (xtionDepthPoints.size()-1)*3;
        Eigen::MatrixXd A(nEq, 4);
        Eigen::MatrixXd  b(nEq, 1);


        double x0 = xtionDepthPoints[0](0);
        double y0 = xtionDepthPoints[0](1);
        double z0 = xtionDepthPoints[0](2) / 1000.0;
        int j=0;
    for(int i=1; i<xtionDepthPoints.size(); ++i){

                double xi = xtionDepthPoints[i](0);
                double yi = xtionDepthPoints[i](1);
                double zi = xtionDepthPoints[i](2) / 1000.0;
                Eigen::Matrix4d R = pointsRotMats[0].inverse() * pointsRotMats[i];


          A(j,0) = x0*z0 - R(0,0)*xi*zi;
          A(j,1) = -R(0,1)*yi*zi;
          A(j,2) = R(0,0)*zi - z0;
          A(j,3) = R(0,1)*zi;
          b(j,0) = R(0,2)*zi + R(0,3);
          j++;


          A(j,0) = -R(1,0)*xi*zi;
          A(j,1) = y0*z0 - R(1,1)*yi*zi;
          A(j,2) = R(1,0)*zi;
          A(j,3) = R(1,1)*zi - z0;
          b(j,0) = R(1,2)*zi + R(1,3);
          j++;


          A(j,0) = -R(2,0)*xi*zi;
          A(j,1) = -R(2,1)*yi*zi;
          A(j,2) = R(2,0)*zi;
          A(j,3) = R(2,1)*zi;
          b(j,0) = -z0 + R(2,2)*zi + R(2,3);
          j++;

        }


    //Eigen::Vector4d sol = (A.transpose()*A).inverse() * A.transpose() * b;
    Eigen::Vector4d sol = A.jacobiSvd(Eigen::ComputeThinU |
Eigen::ComputeThinV).solve(b);
        if(sol(0) == 0 || sol(1) == 0)
                return false;
        mFx = 1/sol(0);
        mFy = 1/sol(1);
        mCx = sol(2)*mFx;
    mCy = sol(3)*mFy;



        return true;
}


bool xtionCalibrator::calibrateExtrinsics(){

        return true;
}



bool xtionCalibrator::writeIntrinsics(std::string fname){

        std::cout << "fx: " << mFx << "  fy: " << mFy << "  cx: " << mCx << "  cy: " << mCy
<< std::endl;
        return true;
}



bool xtionCalibrator::calibrateAll(){

        return calibrateIntrinsics() & calibrateExtrinsics();

}
