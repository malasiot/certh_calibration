import os

home = os.environ['HOME']

if not os.path.exists(home + "/.ros/data"):
   os.makedirs(home + "/.ros/data")

if not os.path.exists(home + "/.ros/data/certh_data"):
   os.makedirs(home + "/.ros/data/certh_data")

if not os.path.exists(home + "/.ros/data/certh_data/calibration_nikon"):
   os.makedirs(home + "/.ros/data/certh_data/calibration_nikon")

if not os.path.exists(home + "/.ros/data/certh_data/calibration_nikon/data_intrinsics"):
   os.makedirs(home + "/.ros/data/certh_data/calibration_nikon/data_intrinsics")

if not os.path.exists(home + "/.ros/data/certh_data/calibration_nikon/data_extrinsics"):
   os.makedirs(home + "/.ros/data/certh_data/calibration_nikon/data_extrinsics")



